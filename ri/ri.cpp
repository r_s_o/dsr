#include "ri.h"

#include <algorithm>
#include <cstring>

namespace rl_interface {

/// Image

Image::Image(size_t aW, const char src[])
   : _w(aW)
{
   try
   {
      static const size_t  CHUNK = 4; // Тетрада (фон, цвет, яркость, символ)
      static const int     BASE  = '0'; // Смещение символов цвета.

      if( strlen(src) % CHUNK )
         throw "Image: wrong src size!";

      const char* c=src;
      while(*c)
      {
         int bg = *c++ - BASE;
         int fg = *c++ - BASE;
         int bold = *c++;
         int symbol = *c++;
         _pixels.push_back( {symbol, bg, fg, escape::HIGHLIGHT==bold ? true : false} );
      }
   }
   catch(...)
   {
      _pixels = { {'B', 0, 0, true}, {'A', 0, 0, true}, {'D', 0, 0, true} };
      _w = 3;
   }
}

Pixel& Image::get(size_t x, size_t y)
{
   return _pixels[y * _w + x];
}


/// View

View::~View() {}

void View::addChild(string child)
{
   if( find(_children.begin(), _children.end(), child) == _children.end() )
      _children.push_back(child);
}

void View::removeChild(string child)
{
   _children.erase( find(_children.begin(), _children.end(), child) );
}

size_t View::fsize(string s)
{
   size_t size = 0;
   auto si = s.begin();
   while( si != s.end() )
   {
      switch(*si)
      {
      case  escape::PREFIX:
      case  escape::HIGHLIGHT:
      case  escape::UNDERLINE:
         ++si;
      default:
         ++size;
         ++si;
      }
   }
   return size;
}


/// TextView

TextView::~TextView() {}


/// Label

Label::~Label() {}


/// ImageView

ImageView::~ImageView() {}


/// FieldView

FieldView::~FieldView() {}


/// Ri

Ri::Ri(const uint aHeight, const uint aWidth)
   : _width(aWidth),
     _height(aHeight)
{
}

Ri::~Ri()
{
}

void Ri::addView(const string& name, View* v)
{
   v->name(name);
   _views[name] = unique_ptr<View>(v);
}

void Ri::removeView(const string& name)
{
   auto v = view(name);
   if(!v)
      return;

   setViewParent(name, string());
   _views.erase( _views.find(name) );
}

shared_ptr<View> Ri::view(const string& name)
{
   auto vi =_views.find(name);
   return vi==_views.end() ? nullptr : vi->second;
}

void Ri::setViewParent(const string& child, const string& parent)
{
   auto c = view(child);
   if(c)
   {
      auto oldP = view(c->parent());
      if(oldP)
         oldP->removeChild(child);

      c->parent(parent);
   }

   auto newP = view(parent);
   if(newP)
      newP->addChild(child);
}

void Ri::clearViews()
{
   _views.clear();
}

void Ri::storeViews()
{
   _storage.push(_views);
   clearViews();
}

void Ri::restoreViews()
{
   clearViews();
   _views = _storage.top();
   _storage.pop();
}


View* Ri::asView(const string& name)
{
   return view(name).get();
}

TextView* Ri::asTextView(const string& name)
{
   return dynamic_cast<TextView*>(view(name).get());
}

Label* Ri::asLabel(const string& name)
{
   return dynamic_cast<Label*>(view(name).get());
}


optional<Point> Ri::relative(const string& name)
{
   auto v = view(name);
   if(!v)
      return nullopt;

   Point point{v->x, v->y};

   if( !v->parent().empty() )
   {
      auto parentPoint = relative(v->parent());
      if(parentPoint)
      {
         point.x += parentPoint->x;
         point.y += parentPoint->y;
      }
   }

   return point;
}

} // namespace rl_interface
