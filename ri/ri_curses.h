#ifndef RI_CURSES_H
#define RI_CURSES_H

#include "ri.h"

namespace rl_interface {

class RiCurses : public Ri
{
public:
   static const int BLACK;
   static const int RED;
   static const int GREEN;
   static const int YELLOW;
   static const int BLUE;
   static const int MAGENTA;
   static const int CYAN;
   static const int WHITE;

protected:
   static constexpr int MAX_COLOURS = 8;
   int colourPairIndex(int back, int fore){ return back * MAX_COLOURS + fore; }

   struct Symbol
   {
      int   symbol      = ' ';
      bool  bold        = false;
      bool  underlined  = false;
   };
   using Symbols = vector<Symbol>;

   static Symbols parse(string s, bool highlighted);

   void renderView(View* v);
   void renderTextView(TextView* v);
   void renderImageView(ImageView* v);
   void renderFieldView(FieldView* v);

public:
   explicit RiCurses(const uint aHeight, const uint aWidth, const bool needColour);
   virtual ~RiCurses() override;

   virtual void render(View* view) override;
   virtual void update() override;

//   virtual void put(string text) override;
//   virtual void put(int y, int x, string text) override;

   virtual int key() override;
};

} // namespace rl_interface

#endif // RI_CURSES_H
