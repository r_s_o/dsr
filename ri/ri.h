#ifndef RI_H
#define RI_H

#include <string>
#include <map>
#include <vector>
#include <stack>
#include <memory>
#include <functional>
#include <optional>

// Потому что всему есть разумный предел.
#if __clang__==1
#pragma clang diagnostic ignored "-Wpadded"
#endif

namespace rl_interface {

using namespace std;

/** Точка. */
struct Point
{
   int  x;
   int  y;
}; // struct Point


/** Прямоугольная область. */
struct Rect: public Point
{
   int  w;
   int  h;

   int bottom() { return y + h; }
   int right () { return x + w; }
}; // class Rect


/** Цвет. */
using Colour = int;


/** Цветное изображение в закодированном формате. */
struct Pixel
{
   int      symbol = ' ';
   Colour   background = 0;
   Colour   foreground = 0;
   bool     bold = false;
}; // struct Pixel

using Pixels = vector<Pixel>;


/** Цветное изображение в закодированном формате. */
class Image
{
protected:
   Pixels   _pixels;
   size_t   _w;

public:
   explicit Image(size_t aW, size_t aH) : _pixels(aW * aH), _w(aW) {}
   explicit Image(size_t aW, const char src[]);
   explicit Image(const Image&)=default;

   size_t w() const { return _w; }
   size_t h() const { return _pixels.size() / _w; }

   Pixel& get(size_t x, size_t y);
}; // class Image


/** Управляющие символы строк. */
namespace escape {
   static const char PREFIX     ='%'; // Следующий символ печатается буквально.
   static const char HIGHLIGHT  ='~'; // Инвертирует режим подсветки.
   static const char UNDERLINE  ='_'; // Инвертирует режим подчёркивания.
}


/** Коды клавиш. */
namespace key {
   static const int  ENTER    = 10;
   static const int  ESC      = 27;
   static const int  BACKSPACE= 263;
   static const int  TAB      = 9;
   static const int  SPACE    = ' ';

   static const int  LEFT     = 260;
   static const int  RIGHT    = 261;
   static const int  UP       = 259;
   static const int  DOWN     = 258;
   static const int  HOME     = 262;
   static const int  END      = 360;
   static const int  PGUP     = 339;
   static const int  PGDOWN   = 338;
   static const int  CENTER   = 69;

   static const int  F1       = 265;
   static const int  F2       = 266;
   static const int  F3       = 267;
   static const int  F4       = 268;
   static const int  F5       = 269;
   static const int  F6       = 270;
   static const int  F7       = 271;
   static const int  F8       = 272;
   static const int  F9       = 273;
   static const int  F10      = 274;
   static const int  F11      = 275;
   static const int  F12      = 276;
}


/** Визуальный элемент. */
struct View: public Rect
{
protected:
   string         _name;
   string         _parent;
   vector<string> _children;

public:
   Colour   background;
   Colour   foreground;
   int      filler = '.';
   bool     visible = true;

public:
   explicit View(int aX, int aY, int aW, int aH,
                 Colour aBackground, Colour aForeground, int aFiller = '.')
      : Rect{aX, aY, aW, aH},
        background(aBackground), foreground(aForeground), filler(aFiller) {}
   virtual ~View();

public:
   string name() const { return _name; }
   void name(const string& aName) { _name = aName; }

   string parent() const { return _parent; }
   void parent(const string& aParent) { _parent = aParent; }

   vector<string>& children() { return _children; }
   void addChild(string child);
   void removeChild(string child);

   /** Работа с форматированным текстом. */
   static size_t fsize(string s);
}; // struct View

using Views = map<string, shared_ptr<View> >;


/** Поле с текстом. */
struct TextView: public View
{
   string   text;
   bool     highlight = false;

   explicit TextView(int aX, int aY, int aW, int aH,
                     Colour aBackground, Colour aForeground,
                     string aText, bool aHighlight = false)
      : View(aX, aY, aW, aH, aBackground, aForeground),
        text(aText), highlight(aHighlight)
   {
      filler = ' ';
   }
   ~TextView();
}; // struct Label


/** Однострочная метка, ширина которой определяется шириной текста. */
struct Label: public TextView
{
   explicit Label(int aX, int aY,
                  Colour aBackground, Colour aForeground,
                  string aText, bool aHighlight = false)
      : TextView(aX, aY, int(fsize(aText)), 1, aBackground, aForeground, aText, aHighlight) {}
   explicit Label(int aX, int aY, int aW,
                  Colour aBackground, Colour aForeground,
                  string aText, bool aHighlight = false)
      : TextView(aX, aY, aW, 1, aBackground, aForeground, aText, aHighlight) {}
   ~Label();
}; // struct Label


/*
struct ListView: public View
{
   explicit Label(int aX, int aY,
                  Colour aBackground, Colour aForeground,
                  string aText, bool aHighlight=false)
      : TextView(aX, aY, fsize(aText), 1, aBackground, aForeground, aText, aHighlight) {}
}; // struct Label
*/


/** Визуальный элемент с изображением. */
struct ImageView: public View
{
   Image image;

   explicit ImageView(int aX, int aY, size_t aW, const char aSource[])
      : View(aX, aY, 1, 1, 0, 0), image(aW, aSource)
   {
      w = int(image.w());
      h = int(image.h());
   }
   ~ImageView();
}; // struct ImageView


/** Поле. */
struct FieldView: public View
{
public:
   /** Функция доступа к "пикселю" картинки. */
   using Access = function<Pixel(int x, int y)>;

protected:
   Access _access;

public:
   explicit FieldView(Access aAccess, int aX, int aY, int aW, int aH)
      : View(aX, aY, aW, aH, 0, 0), _access(aAccess) {}
   ~FieldView();

   Pixel access(int x, int y) { return _access(x, y); }
}; // struct FieldView


class Ri
{
protected:
   bool  _initialized = false;
   uint  _width = 80;
   uint  _height = 24;
   Views _views;

private:
   stack<Views> _storage;

protected:
   using ViewsVector = vector<shared_ptr<View>>;

   optional<Point> relative(const string& name);

public:
   explicit Ri(const uint aHeight, const uint aWidth);
   virtual ~Ri();

   operator bool() const { return _initialized; }
   uint h() const { return _height; }
   uint w() const { return _width; }

   void              addView(const string& name, View* v);
   void              removeView(const string& name);
   shared_ptr<View>  view(const string& name);
   /** Работает строго после добавления child и parent в список вьюх. */
   void              setViewParent(const string& child, const string& parent);
   void              clearViews();
   void              storeViews();
   void              restoreViews();

   View*             asView      (const string& name);
   TextView*         asTextView  (const string& name);
   Label*            asLabel     (const string& name);

   virtual void render(View* view)=0;
   virtual void update()=0;

   virtual int key()=0;
}; // class Ri

} // namespace rl_interface

#endif // RI_H
