cmake_minimum_required(VERSION 3.8)

project(ri VERSION 1 LANGUAGES C CXX)
add_library(${PROJECT_NAME} STATIC "ri.cpp" "ri_curses.cpp")

set_target_properties(
   ${PROJECT_NAME} PROPERTIES
   CXX_STANDARD 20
   CXX_EXTENSIONS OFF
)
