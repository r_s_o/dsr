#include "ri_curses.h"

#include <ncurses.h>
#include <locale.h>
#include <chrono>
#include <thread>

//#include <iostream>

namespace rl_interface {

const int RiCurses::BLACK  =COLOR_BLACK;
const int RiCurses::RED    =COLOR_RED;
const int RiCurses::GREEN  =COLOR_GREEN;
const int RiCurses::YELLOW =COLOR_YELLOW;
const int RiCurses::BLUE   =COLOR_BLUE;
const int RiCurses::MAGENTA=COLOR_MAGENTA;
const int RiCurses::CYAN   =COLOR_CYAN;
const int RiCurses::WHITE  =COLOR_WHITE;

RiCurses::RiCurses(const uint aHeight, const uint aWidth, const bool needColour)
   : Ri(aHeight, aWidth)
{
   using namespace std::chrono_literals;
   this_thread::sleep_for(200ms);

   setlocale(LC_ALL, "");
   initscr();

   unsigned int rows, cols;
   getmaxyx(stdscr, rows, cols);
   if(aHeight>rows || aWidth>cols)
   {
      endwin();
      printf("Your terminal cannot provide (%u x %u).\n", aWidth, aHeight);
      return;
   }
   if(needColour && !has_colors())
   {
      endwin();
      printf("Your terminal does not support colour.\n");
      return;
   }

   start_color();
   use_default_colors();
   curs_set(0);
   noecho();
   keypad(stdscr, true);

   // Инициализировать все сочетания цветов.
   for(int back=0; back<MAX_COLOURS; ++back)
      for(int fore=0; fore<MAX_COLOURS; ++fore)
         init_pair( short(colourPairIndex(back, fore)), short(fore), short(back));

   _initialized = true;
}

RiCurses::~RiCurses()
{
   if(_initialized)
      endwin();
}

RiCurses::Symbols RiCurses::parse(string s, bool highlighted)
{
   Symbols symbols;

   bool h = highlighted;
   bool u = false;

   auto si = s.begin();
   while( si!=s.end() )
   {
      if( escape::PREFIX==*si ) // Следующий символ буквально.
      {
         if( ++si!=s.end() )
         {
            symbols.push_back( {*si, h, u} );
            ++si;
         }
      }
      else if( escape::HIGHLIGHT==*si )
      {
         h = !h; // Простая инверсия.
         ++si;
      }
      else if( escape::UNDERLINE==*si )
      {
         u = !u; // Простая инверсия.
         ++si;
      }
      else
      {
         symbols.push_back( {*si, h, u} );
         ++si;
      }
   }

   return symbols;
}

void RiCurses::renderView(View* v)
{
   auto point = relative(v->name());
   if(!point)
      return;

   int a = colourPairIndex(v->background, v->foreground);
   for(int i=0; i<v->h; ++i)
   {
      for(int j=0; j<v->w; ++j)
      {
         attron( COLOR_PAIR(a) );
         //TODO Заменить на mvaddch (?)
         mvprintw(point->y+i, point->x+j, string(1, v->filler).c_str());
         attroff( COLOR_PAIR(a) );
      }
   }
}

void RiCurses::renderTextView(TextView* v)
{
   auto point = relative(v->name());
   if(!point)
      return;

   Symbols symbols = parse(v->text, v->highlight);

   const int p = COLOR_PAIR(colourPairIndex(v->background, v->foreground));

   auto fill = [&](int i, int j, int a)
   {
      attron(a);
      mvaddch(i, j, v->filler);
      attroff(a);
   };

   int si = 0;
   for(int i=point->y; i<point->y+v->h; ++i) // По строкам
   {
      for(int j=point->x; j<point->x+v->w; ++j) // По столбцам.
      {
         if( si>=symbols.size() ) // Символы закончились, просто заполняем поле.
         {
            fill(i, j, (p | (v->highlight ? A_BOLD : 0)) );
         }
         else
         {
            const Symbol& s=symbols[si];

            if( '\n'==s.symbol ) // Перевод строки?
            {
               const int a = (p | (v->highlight ? A_BOLD : 0));
               while( j < point->x+v->w ) // Добиваем заполнитель до конца строки.
                  fill(i, j++, a);
            }
            else // Настоящий символ.
            {
               const int a = (p | (s.bold ? A_BOLD : 0) | (s.underlined ? A_UNDERLINE : 0));
               attron(a);
               mvaddch(i, j, s.symbol);
               attroff(a);
            }
            ++si;
         }
      }
   }
}

void RiCurses::renderImageView(ImageView* v)
{
   auto point = relative(v->name());
   if(!point)
      return;

   for(int i=0; i<v->h; ++i)
   {
      for(int j=0; j<v->w; ++j)
      {
         Pixel& s = v->image.get(j, i);
         int a = COLOR_PAIR(colourPairIndex(s.background, s.foreground)) | (s.bold ? A_BOLD : 0);
         attron(a);
         mvaddch(point->y+i, point->x+j, s.symbol);
         attroff(a);
      }
   }
}

void RiCurses::renderFieldView(FieldView* v)
{
   auto point = relative(v->name());
   if(!point)
      return;

   for(int i=0; i<v->h; ++i)
   {
      for(int j=0; j<v->w; ++j)
      {
         Pixel s = v->access(j, i);
         int a = COLOR_PAIR(colourPairIndex(s.background, s.foreground)) | (s.bold ? A_BOLD : 0);
         attron(a);
         mvaddch(point->y+i, point->x+j, s.symbol);
         attroff(a);
      }
   }
}

void RiCurses::render(View* v)
{
   // Сначала себя.
   if( TextView* label = dynamic_cast<TextView*>(v) )
      renderTextView(label);
   else if( ImageView* image = dynamic_cast<ImageView*>(v) )
      renderImageView(image);
   else if( FieldView* field = dynamic_cast<FieldView*>(v) )
      renderFieldView(field);
   else
      renderView(v);

   // Затем детей.
   for(auto c: v->children())
   {
      auto cv = view(c);
      if(cv)
         render(cv.get());
   }
}

void RiCurses::update()
{
   clear();
   for(auto& v: _views)
      if(v.second->parent().empty() && v.second->visible)
         render(v.second.get());

   refresh();
}

int RiCurses::key()
{
   return getch();
}

} // namespace rl_interface
