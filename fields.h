#ifndef FIELDS_H
#define FIELDS_H

#include "ds/ds.h"

//#include <optional>

namespace fields {

using namespace ds;

// Общие поля.
static const string MY_ZERO               ="myZero";

// Поля инженера.
static const string MY_WORKSHOP           ="myWorkshop";
static const string MY_WORKSHOP_BASEMENT  ="myWorkshopBasement";
static const string MY_WORKSHOP_STREET    ="myWorkshopStreet";

// Поля солдата.
static const string MY_BARRAKS            ="myBarraks";
static const string MY_BARRACKS_STREET    ="myBarracksStreet";

static const string MY_HOSPITAL  ="myHospital";
static const string MY_LAB       ="myLab";
static const string MY_CELLAR    ="myCellar";
static const string MY_CHURCH    ="myChurch";

const Field::Source& globalMap();
cstring symbolToName(char8_t symbol);

tuple<int, int> fieldNameToGlobalXY(cstring& name);
cstring globalXYToFieldName(int x, int y);

/** Загружает поле из прошитых данных. */
optional<Field> predefinedField(const string& name);

} // namespace fields

#endif // FIELDS_H
