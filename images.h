#ifndef IMAGES_H
#define IMAGES_H

#include <cstddef>

namespace images {

extern const size_t titleW;
extern const char title[];

} // namespace images

#endif // IMAGES_H
