cmake_minimum_required(VERSION 3.8)

project(dsr VERSION 1 LANGUAGES C CXX)
add_executable(${PROJECT_NAME}
   "main.cpp"
   "images.cpp"
   "fields.cpp"
   "ds/ds.cpp"
   "README.md")
add_subdirectory(ri)

target_compile_options(${PROJECT_NAME} PRIVATE -Wall -Wextra -pedantic)

target_link_libraries(${PROJECT_NAME} ri ncursesw)
set_target_properties(
   ${PROJECT_NAME} PROPERTIES
   CXX_STANDARD 20
   CXX_EXTENSIONS OFF
)
