#include "ds.h"

// Архитектурно плохое решение: связь между цветами должна устанавливаться иначе.
#include "../ri/ri_curses.h"

#include <sstream>
#include <fstream>

#include <iostream>

namespace ds {

string sign(const int x)
{
   return (x > 0) ? "+" : ((x < 0) ? "-" : "");
}

Strings split(cstring& str, char delimiter)
{
   //TODO Разобраться с версией на string_view, почему-то сейчас вдвое медленнее.
   Strings strings;
   istringstream iss(str);
   string s;
   while( getline(iss, s, delimiter) )
      strings.push_back(s);
   return strings;
}

// Лень дёргать итераторы
template<typename Container, typename Predicate>
bool any_of(Container c, Predicate p)
{
   return std::any_of(c.cbegin(), c.cend(), p);
}


/// Visualable

Visualable::~Visualable() {}


/// Effect

string Effect::toString() const
{
   static const string ve[]=
   {
      "No effect",            // NONE
      "HP",                   // HP
      "Max HP",               // MAX_HP
      "Ranged attack",        // RANGE_ATTACK
      "Close attack",         // CLOSE_ATTACK
      "Ranged defence",       // RANGE_DEFENCE
      "Close defence",        // CLOSE_DEFENCE
      "Ranged evasion",       // RANGE_EVASION
      "Close evasion",        // CLOSE_EVASION
      "Ranged crit chance",   // RANGE_CRIT
      "Close crit chance",    // CLOSE_CRIT
      "Dmg on touch",         // DAMAGE_ON_TOUCH
      "Dmg over time"         // DAMAGE_OVER_TIME
   };
   return ve[type];
}


/// Item

Visual Item::toVisual(const Type aType)
{
   using namespace rl_interface;
   using R = RiCurses;

   static const Visual vv[]=
   {
      {' ', R::BLACK , R::BLACK  },    // NONE
      {' ', R::BLACK , R::BLACK  },    // PLACEHOLDER
      {'H', R::BLACK , R::GREEN  },    // CONTAINER
      {'A', R::BLACK , R::BLUE   },    // CONTAINER_CLOSED
      {'B', R::BLACK , R::GREEN  },    // BED
      {'T', R::BLACK , R::BLACK  },    // TABLE
      {'|', R::BLACK , R::BLACK  },    // DOOR
      {'#', R::BLACK , R::BLUE   },    // DOOR_CLOSED
      {'<', R::BLACK , R::GREEN  },    // DOOR_UP
      {'<', R::BLACK , R::BLUE   },    // DOOR_UP_CLOSED
      {'>', R::BLACK , R::GREEN  },    // DOOR_DOWN
      {'>', R::BLACK , R::BLUE   },    // DOOR_DOWN_CLOSED
      {'*', R::BLACK , R::RED    },    // FIRE
      {'*', R::BLACK , R::BLACK  },    // FIRE_DEAD
      {'?', R::BLACK , R::YELLOW },    // WEAPON
      {'?', R::BLACK , R::YELLOW },    // OUTFIT
      {'?', R::BLACK , R::YELLOW },    // KEY
      {'X', R::BLACK , R::MAGENTA},    // CORPSE
      {'x', R::BLACK , R::MAGENTA},    // CORPSE_SMALL
   };
   return vv[aType];
}

bool Item::doorable() const
{
   switch(_type)
   {
   case  DOOR_DOWN:
   case  DOOR_UP:
      return true;
   default:
      return false;
   }
   return false;
}

bool Item::pickable() const
{
   switch(_type)
   {
   case  WEAPON:
   case  OUTFIT:
   case  KEY:
   case  CORPSE_SMALL:
      return true;
   default:
      return false;
   }
   return false;
}

bool Item::droppable() const
{
   //TODO
   return true;
}

bool Item::weaponable() const
{
   switch(_type)
   {
   case  WEAPON:
      return true;
   default:
      return false;
   }
   return false;
}

bool Item::outfitable() const
{
   switch(_type)
   {
   case  OUTFIT:
      return true;
   default:
      return false;
   }
   return false;
}

bool Item::freeToMoveInForHero()
{
   switch(_type)
   {
   case  NONE              :
   case  CONTAINER_CLOSED  :
   case  DOOR_CLOSED       :
   case  DOOR_UP_CLOSED    :
   case  DOOR_DOWN_CLOSED  :
      return false;
   default:
      return true;
   }
   return true;
}

bool Item::freeToMoveInForNpc()
{
   switch(_type)
   {
   case  NONE        :
   case  PLACEHOLDER :
   case  FIRE_DEAD   :
   case  WEAPON      :
   case  OUTFIT      :
   case  KEY         :
      return true;
   default:
      return false;
   }
   return true;
}

bool Item::potentiallyOpenable()
{
   switch(_type)
   {
   case  CONTAINER_CLOSED  :
   case  DOOR_CLOSED       :
   case  DOOR_UP_CLOSED    :
   case  DOOR_DOWN_CLOSED  :
      return true;
   default:
      return false;
   }
   return false;
}

bool Item::potentiallyCloseable()
{
   switch(_type)
   {
   case  CONTAINER:
   case  DOOR     :
   case  DOOR_UP  :
   case  DOOR_DOWN:
      return true;
   default:
      return false;
   }
   return false;
}

bool Item::hasTagId(const string& aTagId)
{
   return any_of(split(_tag), [&](auto ts) {
      Strings strings = split(ts, tags::POINT);
      return !strings.empty() && aTagId==strings.front();
   });
}

bool Item::hasTagString(const string& tagString)
{
   return any_of(split(_tag), [&](auto t){ return tagString==t; });
}

string Item::getTagString(const string& aTagId)
{
   //TODO find_if ?

   for(auto s: split(_tag))
   {
      Strings strings = split(s, tags::POINT);
      if( strings.size()>0 && aTagId==strings[0] ) //TODO empty/front
         return s;
   }
   return string();
}

tuple<string, string> Item::parseTagTeleport()
{
   Strings strings = split(getTagString(tags::TELEPORT), tags::POINT);
   if( strings.size()!=3 || strings[0]!=tags::TELEPORT )
      throw "Bad tag exception."; // Плохая собака, плохая.
   return { strings[1], strings[2] };
}

bool Item::open()
{
   switch(_type)
   {
   case  CONTAINER_CLOSED  :  _type = CONTAINER; return true;
   case  DOOR_CLOSED       :  _type = DOOR; return true;
   case  DOOR_UP_CLOSED    :  _type = DOOR_UP; return true;
   case  DOOR_DOWN_CLOSED  :  _type = DOOR_DOWN; return true;
   default:
      return false;
   }
   return false;
}

bool Item::close()
{
   switch(_type)
   {
   case  CONTAINER:  _type = CONTAINER_CLOSED; return true;
   case  DOOR     :  _type = DOOR_CLOSED; return true;
   case  DOOR_UP  :  _type = DOOR_UP_CLOSED; return true;
   case  DOOR_DOWN:  _type = DOOR_DOWN_CLOSED; return true;
   default:
      return false;
   }
   return false;
}


/// Unit

string Unit::toString(Type aType)
{
   static const string vs[]=
   {
      "none",              // NONE
      "engineer",          // ENGINEER
      "soldier",           // SOLDIER
      "medic",             // MEDIC
      "scientist",         // SCIENTIST
      "cossack",           // COSSACK
      "unitologist",       // UNITOLOGIST
      "civilian",          // CIVILIAN
      "child",             // CHILD
      "dog",               // DOG
      "cat",               // CAT
      "rat",               // RAT
      "necromorph",        // NECRO
      "small necromorph"   // NECRO_SMALL
   };
   return vs[aType];
}

string Unit::toString(Gender aGender)
{
   static const string vs[]=
   {
      "male",
      "female"
   };
   return vs[aGender];
}

Visual Unit::toVisual(Type aType)
{
   using namespace rl_interface;
   using R = RiCurses;

   static const Visual vu[]=
   {
      {'?', R::RED   , R::RED    },    // NONE
      {'E', R::BLACK , R::BLACK  },    // ENGINEER
      {'R', R::BLACK , R::BLACK  },    // SOLDIER
      {'M', R::BLACK , R::BLACK  },    // MEDIC
      {'S', R::BLACK , R::BLACK  },    // SCIENTIST
      {'C', R::BLACK , R::BLACK  },    // COSSACK
      {'U', R::BLACK , R::BLACK  },    // UNITOLOGIST
      {'V', R::BLACK , R::CYAN   },    // CIVILIAN
      {'h', R::BLACK , R::BLACK  },    // CHILD
      {'d', R::BLACK , R::BLACK  },    // DOG
      {'c', R::BLACK , R::BLACK  },    // CAT
      {'r', R::BLACK , R::BLACK  },    // RAT
      {'N', R::BLACK , R::RED    },    // NECRO
      {'n', R::BLACK , R::RED    }     // NECRO_SMALL
   };
   return vu[aType];
}

Unit::Scale Unit::toScale(Type aType)
{
   static const Scale su[]=
   {
      SMALL,    // NONE
      LARGE,    // ENGINEER
      LARGE,    // SOLDIER
      LARGE,    // MEDIC
      LARGE,    // SCIENTIST
      LARGE,    // COSSACK
      LARGE,    // UNITOLOGIST
      LARGE,    // CIVILIAN
      SMALL,    // CHILD
      SMALL,    // DOG
      SMALL,    // CAT
      SMALL,    // RAT
      LARGE,    // NECRO
      SMALL     // NECRO_SMALL
   };
   return su[aType];
}

int Unit::rangeAttackOverall()
{
   return _attributes.rangeAttack
         + effects().accumulate(Effect::RANGE_ATTACK)
         + _weapon->effects().accumulate(Effect::RANGE_ATTACK);
}

int Unit::closeAttackOverall()
{
   return _attributes.closeAttack
         + effects().accumulate(Effect::CLOSE_ATTACK)
         + _weapon->effects().accumulate(Effect::CLOSE_ATTACK);
}

int Unit::rangeDefenceOverall()
{
   return _attributes.rangeDefence
         + effects().accumulate(Effect::RANGE_DEFENCE)
         + _weapon->effects().accumulate(Effect::RANGE_DEFENCE);
}

int Unit::closeDefenceOverall()
{
   return _attributes.closeDefence
         + effects().accumulate(Effect::CLOSE_DEFENCE)
         + _weapon->effects().accumulate(Effect::CLOSE_DEFENCE);
}

int Unit::rangeEvasionOverall()
{
   return _attributes.rangeEvasion
         + effects().accumulate(Effect::RANGE_EVASION)
         + _weapon->effects().accumulate(Effect::RANGE_EVASION);
}

int Unit::closeEvasionOverall()
{
   return _attributes.closeEvasion
         + effects().accumulate(Effect::CLOSE_EVASION)
         + _weapon->effects().accumulate(Effect::CLOSE_EVASION);
}

int Unit::rangeCritOverall()
{
   return _attributes.rangeCrit
         + effects().accumulate(Effect::RANGE_CRIT)
         + _weapon->effects().accumulate(Effect::RANGE_CRIT);
}

int Unit::closeCritOverall()
{
   return _attributes.closeCrit
         + effects().accumulate(Effect::CLOSE_CRIT)
         + _weapon->effects().accumulate(Effect::CLOSE_CRIT);
}

void Unit::generateStartingAttributes()
{
   switch(_type)
   {
   case  ENGINEER    :
      _attributes.maxHp = 18 + Ds::random(-2, 6);
      _attributes.rangeAttack = 0;
      _attributes.closeAttack = 3 + Ds::random(-1, 1);
      _attributes.rangeDefence= 3 + Ds::random(-1, 1);
      _attributes.closeDefence= 3 + Ds::random(-2, 4);
      _attributes.rangeEvasion= 1 + Ds::random( 0, 6);
      _attributes.closeEvasion= 1 + Ds::random( 0, 4);
      _attributes.rangeCrit   = 1;
      _attributes.closeCrit   = 1;

      _weapon=make_shared<Item>(new Item(Item::WEAPON, "Plasma cutter", "Yours trusty",
                                         Effects{{Effect::RANGE_ATTACK, 4},
                                                 {Effect::CLOSE_ATTACK, 1}}));
      _outfit=make_shared<Item>(new Item(Item::OUTFIT, "Engineer suit", "Basic engineer outfit, standart issue",
                                         Effects{{Effect::RANGE_DEFENCE, 4},
                                                 {Effect::CLOSE_DEFENCE, 4}}));

      ///!_effects.push_back( {Effect::CLOSE_ATTACK, 42} );

      _inventory.push_back( make_shared<Item>(new Item(Item::WEAPON, "Plasma cutter", "Weak weapon",
                                                       Effects{{Effect::RANGE_ATTACK, 5},
                                                               {Effect::CLOSE_ATTACK, 2}})) );
      // Test
      //      for(int i=0; i<25; ++i)
      //      {
      //         _inventory.push_back( Item(Item::WEAPON, "Plasma cutter "+to_string(i), "Strong weapon",
      //                                    Effects{{Effect::RANGE_ATTACK, 40},
      //                                            {Effect::CLOSE_ATTACK, 20}}) );
      //         _inventory.push_back( Item(Item::OUTFIT, "Suit "+to_string(i), "A suit. Just a suit",
      //                                    Effects{{Effect::DAMAGE_OVER_TIME, Ds::random(-2, 2)}}));
      //      }
      break;

   case  SOLDIER     :
   case  MEDIC       :
   case  SCIENTIST   :
   case  COSSACK     :
   case  UNITOLOGIST :
   case  CIVILIAN    :
      _attributes.maxHp = 18 + Ds::random(-2, 6);
      break;

   case  CHILD       :
      _attributes.maxHp = 6 + Ds::random(-1, 2);
      break;

   case  DOG         :
      _attributes.maxHp = 8 + Ds::random(-1, 1);
      break;

   case  CAT         :
      _attributes.maxHp = 4 + Ds::random(-1, 1);
      break;

   case  RAT         :
      _attributes.maxHp = 2 + Ds::random(-1, 1);
      break;

   case  NECRO       :
      _attributes.maxHp = 18 + Ds::random(-3, 12);
      break;

   case  NECRO_SMALL :
      _attributes.maxHp = 12 + Ds::random(-3, 6);
      break;

   default:
      break;
   }

   _attributes.hp = _attributes.maxHp;
}


/// Cell

Visual Cell::toVisual(Type aType)
{
   using namespace rl_interface;
   using R = RiCurses;

   static const Visual vc[]=
   {
      {' ', R::BLACK , R::BLACK},   // NONE
      {'.', R::YELLOW, R::WHITE},   // MUD
      {'~', R::BLACK , R::GREEN},   // GRASS
      {'*', R::BLACK , R::WHITE},   // STONE
      {'%', R::BLACK , R::WHITE},   // ROCK
      {'~', R::BLUE  , R::WHITE},   // WATER
      {'#', R::BLACK , R::BLACK},   // WALL
      {'.', R::BLACK , R::BLACK}    // FLOORING
   };
   return vc[aType];
}

bool Cell::freeToMoveIn()
{
   switch(_type)
   {
   case  NONE  :
   case  ROCK  :
   case  WATER : //TODO Плавающие персонажи?
   case  WALL  : return false;
   default:
      return true;
   }
   return true;
}


/// Field

Field::Field(const string& aName, const string& aDesc, const Source& src,
             const Items& aItems, const Units& aUnits, int aDx, int aDy)
   : _items(aItems), _units(aUnits),
     _dx(aDx), _dy(aDy), _name(aName), _desc(aDesc)
{
   _w = src.w;
   size_t h = src.h;
   const char* ci = src.data;
   for(size_t i=0; i<h*_w; ++i) //TODO reserve
   {
      switch(*ci++)
      {
      case  '.':  _cells.push_back( Cell(Cell::FLOORING) ); break;
      case  '#':  _cells.push_back( Cell(Cell::WALL) ); break;
      case  'w':  _cells.push_back( Cell(Cell::WATER) ); break;
      case  '_':  _cells.push_back( Cell(Cell::GRASS) ); break;
      case  '*':  _cells.push_back( Cell(Cell::STONE) ); break;
      case  '%':  _cells.push_back( Cell(Cell::ROCK) ); break;
      }
   }

   if(aDx<0)
      _dx = (Ds::W - int(_w)) / 2;
   if(aDy<0)
      _dy = (Ds::H - int(h)) / 2;
}

Field::Field(const string& aName, const string& aDesc, size_t aW, size_t aH)
   : _w(aW), _name(aName), _desc(aDesc)
{
   for(size_t i=0; i<_w*aH; ++i)
   {
      _cells.push_back( Cell(Cell::FLOORING) ); //TODO Optimize.
   }

   _dx = (Ds::W - int(_w)) / 2;
   _dy = (Ds::H - int(h())) / 2;
}

Items Field::itemsAt(const int x, const int y, const Item::Pickability pickability) const
{
   Items selection;
   for(auto ii=_items.begin(); ii!=_items.end(); ++ii)
   {
      //      PItem item=const_cast<Item*>(&*ii); // Ха
      PItem item = *ii;
      if( item->here(x, y) )
      {
         if(pickability==Item::PICKABLE)
         {
            if( item->pickable() )
               selection.push_back(item);
         }
         else
            selection.push_back(item);
      }
   }
   return selection;
}

RawCell Field::cell(const int x, const int y)
{
   if(x<0 || y<0 || x>=int(w()) || y>=int(h()))
      return nullptr;

   return &_cells[y*_w+x];
}

RawCell Field::screenCell(const int screenX, const int screenY)
{
   return cell(screenX-_dx, screenY-_dy);
}

PItem Field::topItemAt(const int x, const int y, const bool visibleOnly)
{
   // Контейнер всегда должен быть "сверху" -
   // мы это имитируем с помощью невидимости его содержимого.
   for(auto ii=_items.rbegin(); ii!=_items.rend(); ++ii)
   {
      if( (*ii)->here(x, y) ) // В этом месте есть предмет.
      {
         if(visibleOnly)
         {
            if( (*ii)->visible() )
               return *ii;
         }
         else
            return *ii;
      }
   }
   return nullptr;
}

PItem Field::screenTopItemAt(const int screenX, const int screenY, const bool visibleOnly)
{
   return topItemAt(screenX-_dx, screenY-_dy, visibleOnly);
}

PItem Field::anyOf(const Item::Type aType)
{
   for(auto ii=_items.begin(); ii!=_items.end(); ++ii)
      if( aType==(*ii)->type() )
         return *ii;

   return nullptr;
}

void Field::removeItem(const PItem pItem)
{
   //TODO: ?
   auto comp = [pItem](const PItem i){ return pItem==i; };

   _items.erase( remove_if(_items.begin(), _items.end(), comp), _items.end() );
}

RawUnit Field::topUnitAt(const int x, const int y, const bool visibleOnly)
{
   // Тупо перебираем юниты с конца списка.
   for(auto ui=_units.rbegin(); ui!=_units.rend(); ++ui)
   {
      if( ui->here(x, y) ) // В этом месте есть юнит.
      {
         if(visibleOnly)
         {
            if( ui->visible() )
               return &*ui;
         }
         else
            return &*ui;
      }
   }
   return nullptr;
}

RawUnit Field::screenTopUnitAt(const int screenX, const int screenY, const bool visibleOnly)
{
   return topUnitAt(screenX-_dx, screenY-_dy, visibleOnly);
}

bool Field::makeCorpse(RawUnit u)
{
   if(!u)
      return false;

   auto ii = find_if(_units.begin(), _units.end(), [&](auto i){ return i.name()==u->name(); } );
   if(ii!=_units.end()) //TODO Hate this one
   {
      // Добавить имущество свежеубиенного и его труп.
      int ux = u->x();
      int uy = u->y();
      Items& inventory = ii->inventory(); // Вещи
      for(auto item: inventory)
      {
         item->xy(ux, uy);
         _items.push_back(item);
      }
      // Тушка
      PItem corpse =
            shared(new Item
                   {
                      u->scale()==Unit::LARGE ? Item::CORPSE : Item::CORPSE_SMALL, // Большой либо маленький труп
                      ux, uy,
                      u->name()+"%_carcass"
                      //TODO u->desc()
                      //TODO Effects: разложение, пригодность в пищу етс
                   });
      _items.push_back(corpse);

      _units.erase(ii);
      return true;
   }
   return false;
}

bool Field::tryToOpen(const int aX, const int aY, const Items& inventory, string& reason)
{
   Items selection = itemsAt(aX, aY, Item::ANY);
   for(auto si: selection)
   {
      //TODO Разобраться с ключами

      //      if( si->hasTagId(tags::LOCK) )
      //      {
      //      }

      if( si->potentiallyOpenable() ) // В принципе может открываться.
      {
         //         if( si->hasTagId(LOCK) ) // Для открытия нужен ключ.
         string tagString = si->getTagString(LOCK);
         if( tagString.empty() ) // Для открытия не нужен ключ.
         {
            si->open();
            reason = "Opened, no key was needed.";
            return true;
         }
         else // Ключ нужен.
         {
            if( inventory.hasTagString(tagString) ) // Есть подходящий ключ.
            {
               si->open();
               reason = "Opened with a certain key.";
               return true;
            }
            else // Нет такого ключа.
            {
               reason = "A certain key is needed, and you don't have one.";
               return false;
            }
         }
      }
   }
   reason = "Nothing to open there.";
   return false;
}

bool Field::tryToClose(const int aX, const int aY, string& reason)
{
   Items selection = itemsAt(aX, aY, Item::ANY);
   for(auto si: selection)
   {
      if( si->potentiallyCloseable() ) // Может закрываться.
      {
         si->close();
         reason = "Closed.";
         return true;
      }
   }
   reason = "Nothing to close there.";
   return false;
}


/// World

size_t World::_fieldSuffix=0;

void World::setGlobalMapLoaders(GlobalMapLoader aGlobalMapLoader, SymbolToName aSymbolToName)
{
   globalMapLoader = aGlobalMapLoader;
   symbolToName = aSymbolToName;

   const Field::Source& source = globalMapLoader();

   for(size_t i=0; i<World::H; ++i)
   {
      for(size_t j=0; j<World::W; ++j)
      {
         char8_t symbol = source.data[i * World::W + j];
         if(symbol != '.')
         {
            string name = symbolToName(symbol);
            _explored[i][j] = {name, false};
            //TODO Load the field into _fields?..
         }
      }
   }
}

void World::explore(cstring& aName)
{
   auto[x, y] = fieldNameToGlobalXY(aName);

   // It is ok not to find a field on the global map.
   if(x>-1 && y>-1)
      _explored[y][x] = {aName, true};
}

int World::exploredCount() const
{
   int count = 0;
   for(size_t i=0; i<H; ++i) // Dumb & ineffective, jwdo.
   {
      for(size_t j=0; j<W; ++j)
      {
         if(_explored[i][j].explored)
            count++;
      }
   }
   return count;
}

tuple<int, int> World::fieldNameToXY(cstring& name)
{
   for(size_t i=0; i<H; ++i) //TODO Fix this monstrocity.
   {
      for(size_t j=0; j<W; ++j)
      {
         if(_explored[i][j].name==name)
            return {j, i};
      }
   }
   return {-1, -1};
}

optional<Field> World::field(cstring& aName)
{
   // Сперва поищем в уже загруженных.
   auto fi = _fields.find(aName);
   if( _fields.end()!=fi )
      return {fi->second};

   // Не нашли - поищем в прошитых ресурсах.
   return predefinedLoader(aName);
}

Field World::generateField(string& newName, size_t aW, size_t aH)
{
   newName = "AutogeneratedField" + to_string(_fieldSuffix++);

   Field f(newName, newName, aW, aH);

   // Соответствие соседям.

   return f;
}


/// Ds

mt19937 Ds::rng( random_device{}() );

Ds::Ds()
   : hero(Unit::ENGINEER, Unit::PROTAGONIST, Unit::Protagonist{},
          Unit::MALE, "Isaac", 0, 0) {}

void Ds::setFieldByName(cstring& aName)
{
   auto f = world.field(aName);
   if(f)
   {
      setField(f.value());
      world.explore(aName);
   }
   else
      throw "Wrong field name!";
}

void Ds::setField(const Field& aField)
{
   field = aField;
}

bool Ds::placeHero()
{
   PItem placeholder = field.anyOf(Item::PLACEHOLDER);
   if(!placeholder)
      return false;

   hero.x(placeholder->x());
   hero.y(placeholder->y());
   return true;
}

void Ds::loadField(cstring& aFieldName)
{
   if( aFieldName!=field.name() ) // Надо подгрузить новую карту.
   {
      world.field(field.name(), field); // Но сперва закешировать текущую.
      setFieldByName(aFieldName);
   }
}

void Ds::teleportToItem(cstring& aField, const string& aItem)
{
   loadField(aField);

   PItem place = field.items().item(aItem); // И переставить героя в новое место.
   if( !place )
      throw "No such item.";
   hero.xy(place->x(), place->y());
}

void Ds::teleportToField(cstring& aField, int aFieldX, int aFieldY, const int x, const int y)
{
   loadField(aField);
   int newX = (x!=-1) ? x : field.w() - 1;
   int newY = (y!=-1) ? y : field.h() - 1;
   hero.xy(newX, newY);
}

bool Ds::heroAction(const Action a, string& reason)
{
   Direction d = toDirection(a);
   switch(a)
   {
   case  WAIT: break;

   case  STEP_L   :
   case  STEP_R   :
   case  STEP_U   :
   case  STEP_D   :
   case  STEP_LU  :
   case  STEP_LD  :
   case  STEP_RU  :
   case  STEP_RD  :
      if( canHeroMoveIn(hero.x() + d.x, hero.y() + d.y, reason) )
      {
         reason = "";
         hero.x(hero.x() + d.x);
         hero.y(hero.y() + d.y);
         return true;
      }
      else
      {
         string newFieldName;
         int newHeroX = hero.x();
         int newHeroY = hero.y();
         int newFieldX, newFieldY;
         if( canHeroTransit(hero.x() + d.x, hero.y() + d.y,
                            newFieldName, newFieldX, newFieldY,
                            newHeroX, newHeroY) )
         {
            reason="CAN TRANSIT TO " + newFieldName +
                   ": " + to_string(newHeroX) +
                   ", " + to_string(newHeroY);
            teleportToField(newFieldName, newFieldX, newFieldY, newHeroX, newHeroY);
            return true;
         }
         else
            reason="UNABLE TO TRANSIT!";
      }
      return false;

   case  ATTACK_L :
   case  ATTACK_R :
   case  ATTACK_U :
   case  ATTACK_D :
   case  ATTACK_LU:
   case  ATTACK_LD:
   case  ATTACK_RU:
   case  ATTACK_RD:
      return attack(hero, hero.x() + d.x, hero.y() + d.y, reason);

   case  OPEN_L   :
   case  OPEN_R   :
   case  OPEN_U   :
   case  OPEN_D   :
   case  OPEN_LU  :
   case  OPEN_LD  :
   case  OPEN_RU  :
   case  OPEN_RD  :
      return field.tryToOpen(hero.x() + d.x, hero.y() + d.y, hero.inventory(), reason);

   case  CLOSE_L   :
   case  CLOSE_R   :
   case  CLOSE_U   :
   case  CLOSE_D   :
   case  CLOSE_LU  :
   case  CLOSE_LD  :
   case  CLOSE_RU  :
   case  CLOSE_RD  :
      return field.tryToClose(hero.x() + d.x, hero.y() + d.y, reason);

   case  SHOOT_L  :
   case  SHOOT_R  :
   case  SHOOT_U  :
   case  SHOOT_D  :
   case  SHOOT_LU :
   case  SHOOT_LD :
   case  SHOOT_RU :
   case  SHOOT_RD :
      break;
      //   default: //TODO
      //      break;
   }
   return true;
}

Ds::Direction Ds::toDirection(Action a)
{
   switch(a)
   {
   case  WAIT     : return {0, 0};
   case  STEP_L   :
   case  SHOOT_L  :
   case  OPEN_L   :
   case  ATTACK_L :
   case  CLOSE_L  : return {-1,  0};
   case  STEP_R   :
   case  SHOOT_R  :
   case  OPEN_R   :
   case  ATTACK_R :
   case  CLOSE_R  : return {+1,  0};
   case  STEP_U   :
   case  SHOOT_U  :
   case  OPEN_U   :
   case  ATTACK_U :
   case  CLOSE_U  : return { 0, -1};
   case  STEP_D   :
   case  SHOOT_D  :
   case  OPEN_D   :
   case  ATTACK_D :
   case  CLOSE_D  : return { 0, +1};
   case  STEP_LU  :
   case  SHOOT_LU :
   case  OPEN_LU  :
   case  ATTACK_LU:
   case  CLOSE_LU : return {-1, -1};
   case  STEP_LD  :
   case  SHOOT_LD :
   case  OPEN_LD  :
   case  ATTACK_LD:
   case  CLOSE_LD : return {-1, +1};
   case  STEP_RU  :
   case  SHOOT_RU :
   case  OPEN_RU  :
   case  ATTACK_RU:
   case  CLOSE_RU : return {+1, -1};
   case  STEP_RD  :
   case  SHOOT_RD :
   case  OPEN_RD  :
   case  ATTACK_RD:
   case  CLOSE_RD : return {+1, +1};
   }
   return {0, 0};
}

Items Ds::itemsAtHero(const Item::Pickability pickability)
{
   return field.itemsAt(hero.x(), hero.y(), pickability);
}

PItem Ds::doorAtHero()
{
   auto vi = itemsAtHero(Item::ANY);
   auto ii = find_if(vi.cbegin(), vi.cend(), [](auto i){ return i->doorable(); } );
   return ii!=vi.end() ? *ii : nullptr;
}

bool Ds::drop(PItem item, const int x, const int y)
{
   item->xy(x, y);
   field.items().push_back(item);
   return true;
}

bool Ds::doorable(const Items& items)
{
   return any_of(items, [](auto ii){ return ii->doorable(); } );
}

bool Ds::pickable(const Items& items)
{
   return any_of(items, [](auto ii){ return ii->pickable(); } );
}

bool Ds::pickableAtHeroPosition()
{
   return !field.itemsAt(hero.x(), hero.y(), Item::PICKABLE).empty();
}

bool Ds::attack(Unit& attacker, const int aX, const int aY, string& reason)
{
   RawUnit victim = field.topUnitAt(aX, aY, false);
   if(!victim && hero.here(aX, aY)) // Возможно, мы атакуем протагониста - а он вне списка.
      victim = &hero;

   if(victim && !victim->dead())
   {
      Unit::Attributes& aa = attacker.attributes();
      Unit::Attributes& va = victim->attributes();

      if( random(0, 100) > va.closeEvasion )
      {
         reason = attacker.name()+" attacked "+victim->name();
         int damage = aa.closeAttack;
         if( random(0, 100) < aa.closeCrit )
            reason += " critically";
         else
            damage -= max(va.closeDefence, 1) / 100.0;
         if(damage>0)
         {
            int variance = max(aa.closeAttack/10, 1);
            va.hp -= (damage + random(-variance, variance));
            reason += " for "+to_string(damage)+" hp";
            if(victim->dead())
            {
               reason += ", killing the victim."; //TODO пол, возраст (?)
               if(victim!=&hero)
                  field.makeCorpse(victim);
               //TODO Replace with a corpse
            }
            else
               reason += ".";
         }
         else
            reason += " in vain. ";
      }
      else
         reason=victim->name()+" has evaded.";
   }
   else
      //TODO Разрушаемые стены/мебель/контейнеры?
      reason = "Nothing to attack there.";

   return false;
}

string Ds::mind(Unit& u)
{
   //TODO Refactor later.
   if(u.dead())
      return "";

   string reason;
   switch(u.purpose())
   {
   case  Mindable::BYSTANDER: break;
   case  Mindable::PROTAGONIST: break;
   case  Mindable::WANDERER:
      { // Перемещаемся в пределах заданного прямоугольника.
         auto m = get<Mindable::Wanderer>(u.mindset());
         if(random(0, m.fussiness) < m.fussiness)
            break;

         auto vd = gatherDirections(u);
         auto d = vd[random(0, vd.size()-1)];
         int newX = u.x()+d.x;
         int newY = u.y()+d.y;
         if(newX>=m.left && newY>=m.top && newX<=m.right && newY<=m.bottom)
         {
            u.xy(u.x() + d.x, u.y() + d.y);
            if(random(0, 4) > 3) // Иногда даже сообщаем.
               reason = u.name() + " wanders aimlessly. ";
         }
      }
      break;
   case  Mindable::BRUTE:
      {
         //TODO Момент столкновения либо выстрела в протагониста
         if( abs( u.x() - hero.x())<2 && abs( u.y() - hero.y())<2 )
         {
            attack(u, hero.x(), hero.y(), reason);
         }
         else
         {
            // Гонимся за протагонистом...
            if( get<Mindable::Brute>(u.mindset()).smart ) // ...по уму;
            {
               //TODO
            }
            else // ...по прямой.
            {
               auto dx = u.x() > hero.x() ? -1 : 1;
               auto dy = u.y() > hero.y() ? -1 : 1;
               if( canNpcMoveIn(u.x() + dx, u.y() + dy) )
                  u.xy(u.x() + dx, u.y() + dy);
               else if( canNpcMoveIn(u.x() + dx, u.y()) )
                  u.xy(u.x() + dx, u.y());
               else if( canNpcMoveIn(u.x(), u.y() + dy) )
                  u.xy(u.x(), u.y() + dy);
            }
         }
      }
      break;

      //      AMBUSHER,
      //      COWARD,
      //      HOARDER,
   }
   return reason;
}

Ds::Directions Ds::gatherDirections(const Unit& u)
{
   Directions vd;
   int x = u.x();
   int y = u.y();
   for(int i=-1; i<=1; i++)
      for(int j=-1; j<=1; j++)
      {
         //?         if(i==0 && j==0) // Пропускаем данную клетку.
         //            continue;
         if( canNpcMoveIn(x + i, y + j) )
            vd.push_back( {i, j} );
      }
   return vd;
}

bool Ds::canHeroMoveIn(const int aX, const int aY, string& reason)
{
   const RawCell c = field.cell(aX, aY);
   if( !c || !c->freeToMoveIn() ) // Сама клетка допускает вхождение.
   {
      reason = "You shall not pass.";
      return false;
   }

   Items selection = field.itemsAt(aX, aY, Item::UNPICKABLE); // И в ней нет непроходимого предмета.
   for(auto si: selection)
      if( !si->freeToMoveInForHero() )
      {
         reason = "Open it before entering it ([~o~]).";
         return false;
      }

   if( field.topUnitAt(aX, aY, false) ) // В ней кто-то есть.
      return false;

   return true;
}

bool Ds::canHeroTransit(const int aX, const int aY,
                        string& newFieldName, int& newFieldX, int& newFieldY,
                        int& newHeroX, int& newHeroY)
{
   // Координаты текущего поля.
   auto[fieldX, fieldY] = world.fieldNameToXY(field.name());
   newFieldX = fieldX;
   newFieldY = fieldY;
   {
      if(aX < 0)
      {
         newFieldX--;
         newHeroX = -1;
      }
      else if(aX >= int(field.w()))
      {
         newFieldX++;
         newHeroX = 0;
      }
      if(aY < 0)
      {
         newFieldY--;
         newHeroY = -1;
      }
      else if(aY >= int(field.h()))
      {
         newFieldY++;
         newHeroY = 0;
      }

      // А мир не закончился ли?..
      if(newFieldX<0 || newFieldX>=int(World::W) || newFieldY<0 || newFieldY>=int(World::H))
         return false;
   }

   // Вроде всё нормально, можно переходить.
   const World::Explored& e = world.explored(newFieldX, newFieldY);
   if( e.name!=field.name() )
   {
      newFieldName = e.name;
      return true;
   }

   return false;
}

bool Ds::canNpcMoveIn(const int aX, const int aY)
{
   const RawCell c = field.cell(aX, aY);
   if( !c || !c->freeToMoveIn() ) // Клетка в принципе не допускает вхождение.
      return false;

   Items selection = field.itemsAt(aX, aY, Item::UNPICKABLE); // И в ней нет непроходимого предмета.
   for(auto si: selection)
      if( !si->freeToMoveInForNpc() )
         return false;

   if( field.topUnitAt(aX, aY, false) ) // Там кто-то есть.
      return false;

   return true;
}

string Ds::toText(const Effects& effects,
                  const string& valueAffix, const string& glue)
{
   string s;
   for(size_t i=0; i<effects.size(); ++i)
   {
      const Effect& e = effects.at(i);
      s += e.toString() + ": " +
           valueAffix + sign(e.value) + to_string(abs(e.value)) + valueAffix +
           glue;
   }
   return s;
}

void Ds::dump()
{
   ofstream os("dump.txt", std::ofstream::out);

   os<<"WORLD: "<<world.fields().size()<<" field(s) cached\n";
   for(auto mi: world.fields())
   {
      os<< mi.first <<endl;
   }
   os<<"\n";

   os<<"FIELD: "<<field.name()<<"\n";
   for(auto& i: field.items())
      os<<i->name()<<"\n";
   os<<"\n";

   os<<"UNIT: "<<hero.name()<<"\n";
   for(auto& i: hero.inventory())
      os<<i->name()<<"\n";
   os<<"\n";

   os.close();
}

} // namespace ds
