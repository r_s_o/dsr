#ifndef TAGS_H
#define TAGS_H

#include <string>

namespace tags {

using namespace std;

static const char SPLITTER = '$';
static const char POINT    = '.';

static const string LOCK     = "LOCK";
static const string TELEPORT = "TELEPORT";

inline string id(const string field, const string object)
{
   return field + POINT + object;
}

inline string lock(const string field, const string object)
{
   return LOCK + POINT + id(field, object);
}

inline string teleport(const string field, const string object)
{
   return TELEPORT + POINT + id(field, object);
}

} // namespace tags

#endif // TAGS_H
