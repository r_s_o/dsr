#ifndef DS_H
#define DS_H

#include "tags.h"

#include <vector>
#include <map>
#include <tuple>
#include <random>
#include <algorithm>
#include <functional>
#include <memory>
#include <variant>
#include <optional>

#if __clang__==1
#pragma clang diagnostic ignored "-Wpadded"
#endif

namespace ds {

using namespace std;
using namespace tags;

// Работа со строками, в том числе тегами.

using cstring = const std::string;

using Strings = vector<string>;

Strings split(cstring& str, char delimiter = SPLITTER);
string sign(const int x);


/** Визуальное представление элемента. */
struct Visual
{
   int symbol;
   int background;
   int foreground;
};

static constexpr char HERO_SYMBOL = '@';


/** Объект, идентифицируемый геометрическим положением. */
class Placeable
{
protected:
   int _x, _y;

public:
   explicit Placeable(int aX, int aY) : _x(aX), _y(aY) {}

   int x() const { return _x; }
   void x(const int aX) { _x = aX; }

   int y() const { return _y; }
   void y(const int aY) { _y = aY; }

   void xy(const int aX, const int aY) { _x = aX; _y = aY; }

   bool here(const int aX, const int aY) const { return aX==_x && aY==_y; }
}; // class Placeable


/** Объект, обладающий визуальными свойствами. */
class Visualable
{
public:
   Visualable()=default;
   Visualable(const Visualable&)=default;
   virtual ~Visualable();

   Visualable& operator=(const Visualable&)=default;

   virtual Visual visual() const=0;
}; // class Visualable


/** Эффект. */
struct Effect
{
   enum Type
   {
      NONE = 0,
      HP,
      MAX_HP,
      RANGE_ATTACK,
      CLOSE_ATTACK,
      RANGE_DEFENCE,
      CLOSE_DEFENCE,
      RANGE_EVASION,
      CLOSE_EVASION,
      RANGE_CRIT,
      CLOSE_CRIT,
      DAMAGE_ON_TOUCH,
      DAMAGE_OVER_TIME
   };

   Type  type;
   int   value;

   bool is(const Type aType) const { return aType==type; }
   string toString() const;
}; // struct Effect


/** Список эффектов. Может быть навешен на юнит и на предмет. */
class Effects: public vector<Effect>
{
public:
   using vector::vector;

   auto summator(const Effect::Type aType)
   {
      return [aType](int acc, const Effect& e)
      {
         return e.is(aType) ? acc + e.value : acc;
      };
   }

   int accumulate(const Effect::Type aType)
   {
      return std::accumulate(begin(), end(), 0, summator(aType));
   }
}; // class Effects


/** Объект, обладающий набором эффектов. */
class Effectable
{
protected:
   Effects  _effects;

public:
   Effectable()=default;
   Effectable(const Effects& aEffects) : _effects(aEffects) {}

   Effects& effects() { return _effects; }
}; // class Effectable


/** Объект, обладающий интеллектом. */
class Mindable
{
public:
   enum Purpose
   {
      BYSTANDER = 0,
      PROTAGONIST,
      WANDERER,
      BRUTE,
      AMBUSHER,
      COWARD,
      HOARDER
   };

   struct Bystander {};
   struct Protagonist {};
   struct Wanderer { int left, top, right, bottom; int fussiness; };
   struct Brute { bool smart; };
   struct Ambusher {};
   struct Coward {};
   struct Hoarder {};

   using Mindset = variant<Bystander, Protagonist, Wanderer, Brute, Ambusher, Coward, Hoarder>;

public:
   Mindable()=default;

   virtual Purpose purpose() const =0;
   virtual Mindset mindset() const =0;
}; // class Mindable


/** Предмет. */
class Item: public Placeable, public Visualable, public Effectable
{
public:
   enum Type
   {
      NONE              = 0,
      PLACEHOLDER       = 1,
      CONTAINER         = 2,
      CONTAINER_CLOSED  = 3,
      BED               = 4,
      TABLE             = 5,
      DOOR              = 6,
      DOOR_CLOSED       = 7,
      DOOR_UP           = 8,
      DOOR_UP_CLOSED    = 9,
      DOOR_DOWN         = 10,
      DOOR_DOWN_CLOSED  = 11,
      FIRE              = 12,
      FIRE_DEAD         = 13,
      WEAPON            = 14,
      OUTFIT            = 15,
      KEY               = 16,
      CORPSE            = 17,
      CORPSE_SMALL      = 18
   };

   enum Pickability
   {
      PICKABLE,
      UNPICKABLE,
      ANY
   };

   enum Edibility //TODO
   {
      UNEDIBLE,
      EDIBLE,
      EDIBLE_BUT_IMMORAL
   };

   static Visual toVisual(const Type aType);

private:
   Type     _type = NONE;
   string   _name;
   string   _desc;
   string   _tag;
   bool     _visible = true;
   //TODO Разобраться с весом.
   int      _weight = 500; // В граммах.

public:
   explicit Item(const Item* aItem)
      : Placeable(*aItem), Effectable(*aItem),
      _type(aItem->type()),
      _name(aItem->name()),
      _desc(aItem->desc()),
      _tag(aItem->tag()),
      _visible(aItem->visible())
   {}

   explicit Item(Type aType) : Placeable(0, 0), _type(aType) {}

   explicit Item(Type aType, string aName, string aDesc, const Effects& aEffects)
      : Placeable(0, 0), Effectable(aEffects), _type(aType), _name(aName), _desc(aDesc) {}

   Item(Type aType, int aX, int aY, string aName, string aDesc="", bool aVisible = true)
      : Placeable(aX, aY), _type(aType), _name(aName), _desc(aDesc), _visible(aVisible) {}

   Item(Type aType, int aX, int aY, string aName, const Effects& aEffects)
      : Placeable(aX, aY), Effectable(aEffects), _type(aType),
        _name(aName), _desc(""), _visible(true) {}

   Item(Type aType, int aX, int aY, string aName, string aDesc, const Effects& aEffects)
      : Placeable(aX, aY), Effectable(aEffects), _type(aType),
        _name(aName), _desc(aDesc), _visible(true) {}

   bool nothing() const { return NONE==_type; }
   bool doorable() const;
   bool pickable() const;
   bool droppable() const;
   bool weaponable() const;
   bool outfitable() const;

   Type type() const { return _type; }
   void type(const Type aType) { _type = aType; }

   string name() const { return _name; }
   void name(const string aName) { _name = aName; }

   string desc() const { return _desc; }
   void desc(const string aDesc) { _desc = aDesc; }

   bool visible() const { return _visible; }
   void visible(const bool aVisible) { _visible = aVisible; }

   string tag() const { return _tag; }
   void tag(const string aTag) { _tag = aTag; }

   bool here(const int aX, const int aY) const { return aX==_x && aY==_y; }
   bool freeToMoveInForHero();
   bool freeToMoveInForNpc();
   bool potentiallyOpenable();
   bool potentiallyCloseable();
   bool hasTagId(const string& aTagId);
   bool hasTagString(const string& tagString);
   string getTagString(const string& aTagId);
   bool open();
   bool close();
   tuple<string, string> parseTagTeleport();

   Visual visual() const override { return toVisual(_type); }
}; // class Item

using PItem = shared_ptr<Item>;
using WItem = weak_ptr<Item>;

inline PItem shared(Item* item)
{
   return make_shared<Item>(item);
}


/** Список предметов, инвентарь, содержимое контейнера. */
class Items: public vector<PItem>
{
public:
   using vector::vector;

   bool has(const Item::Type aType) const
   {
      return find_if(begin(), end(),
                     [aType](const PItem i){ return i->type()==aType; }) != end();
   }

   bool has(const Item::Type aType, const string aTag) const
   {
      auto predicate = [aType, aTag](const PItem i)
      {
         return i->type()==aType && i->tag()==aTag;
      };

      return find_if(begin(), end(), predicate) != end();
   }

   bool hasTagString(string tagString) const
   {
      for(auto ii: *this)
         if( ii->hasTagString(tagString) )
            return true;
      return false;
   }

   PItem item(const string aName)
   {
      auto ii = find_if(begin(), end(), [aName](const PItem i){ return aName==i->name(); });
      return ii!=end() ? *ii : nullptr;
   }
}; // class Items


/** "Юнит" - активный житель игрового мира. */
class Unit: public Placeable, public Visualable, public Effectable, public Mindable
{
public:
   enum Type
   {
      NONE        = 0,
      ENGINEER    = 1,
      SOLDIER,
      MEDIC,
      SCIENTIST,
      COSSACK,
      UNITOLOGIST,
      CIVILIAN,
      CHILD,
      DOG,
      CAT,
      RAT,
      NECRO,
      NECRO_SMALL
   };

   static Visual toVisual(Type aType);

   enum Gender
   {
      MALE = 0,
      FEMALE
   };

   enum Scale
   {
      SMALL = 0,
      LARGE = 1
   };
   static Scale toScale(Type aType);

   static constexpr int MAX_NAME = 24;

   static string toString(Type aType);
   static string toString(Gender aGender);

   struct Attributes
   {
      int   maxHp          = 1;
      int   hp             = 1;
      int   rangeAttack    = 0;
      int   closeAttack    = 0;
      int   rangeDefence   = 0; // %, на который снижается урон вдали
      int   closeDefence   = 0; // %, на который снижается урон вблизи
      int   rangeEvasion   = 1; // Шанс на полное уклонение вдали, %, <=95%
      int   closeEvasion   = 1; // Шанс на полное уклонение вблизи, %, <=95%
      int   rangeCrit      = 1; // Шанс критического урона (не учитывает защиту) вдали, %
      int   closeCrit      = 1; // Шанс критического урона (не учитывает защиту) вблизи, %
   };

private:
   Type        _type = NONE;
   Purpose     _purpose = BYSTANDER;
   Mindset     _mindset = Bystander{};
   Gender      _gender = MALE;
   string      _name;
   //TODO string      _desc;
   bool        _visible = true;

   Attributes  _attributes;
   Items       _inventory;
   PItem       _weapon;
   PItem       _outfit;

public:
   explicit Unit(const Unit* aUnit)
      : Placeable(*aUnit), Effectable(*aUnit), Mindable(*aUnit),
        _type(aUnit->type()), _purpose(aUnit->purpose()), _mindset(aUnit->mindset()),
        _gender(aUnit->gender()), _name(aUnit->name()),
        _visible(aUnit->visible()),
        _attributes(aUnit->attributes()),
        _inventory(aUnit->inventory()),
        _weapon(aUnit->weapon()),
        _outfit(aUnit->outfit())
   {}

   Unit(Type aType, Purpose aPurpose, Mindset aMindset, Gender aGender,
        string aName, int aX, int aY, const Items& aInventory = Items{})
      : Placeable(aX, aY), _type(aType), _purpose(aPurpose), _mindset(aMindset),
        _gender(aGender), _name(aName),
        _inventory(aInventory),
        _weapon(make_shared<Item>(new Item{Item::NONE})),
        _outfit(make_shared<Item>(new Item{Item::NONE}))
   {
      generateStartingAttributes();
   }

   Type type() const { return _type; }
   void type(const Type aType) { _type = aType; }

   Purpose purpose() const override { return _purpose; }
   void purpose(const Mindable::Purpose aPurpose) { _purpose = aPurpose; }

   Mindset mindset() const override { return _mindset; }
   void mindset(const Mindable::Mindset aMindset) { _mindset = aMindset; }

   Gender gender() const { return _gender; }
   void gender(const Gender aGender) { _gender = aGender; }

   string name() const { return _name; }
   void name(const string aName) { _name = aName; }

   bool visible() const { return _visible; }
   void visible(const bool aVisible) { _visible = aVisible; }

//   bool dead() const { return _attributes.hp<=0; }

   const Attributes& attributes() const { return _attributes; }
   Attributes& attributes() { return _attributes; }
   const Items& inventory() const { return _inventory; }
   Items& inventory() { return _inventory; }
   const PItem weapon() const { return _weapon; }
   PItem weapon() { return _weapon; }
   const PItem outfit() const { return _outfit; }
   PItem outfit() { return _outfit; }
   bool dead() { return _attributes.hp <= 0; }

   int rangeAttackOverall  ();
   int closeAttackOverall  ();
   int rangeDefenceOverall ();
   int closeDefenceOverall ();
   int rangeEvasionOverall ();
   int closeEvasionOverall ();
   int rangeCritOverall    ();
   int closeCritOverall    ();

   void generateStartingAttributes();

   Visual visual() const override { return toVisual(_type); }
   Scale scale() const { return toScale(_type); }
}; // class Unit

using PUnit = shared_ptr<Unit>;
using WUnit = weak_ptr<Unit>;

using RawUnit = Unit*;
using Units = vector<Unit>;


/** "Клетка" - элемент игрового поля. */
struct Cell: public Visualable
{
public:
   enum Type
   {
      NONE        = 0,
      MUD         = 1,
      GRASS       = 2,
      STONE       = 3,
      ROCK        = 4,
      WATER       = 5,
      WALL        = 6,
      FLOORING    = 7
   };

   static Visual toVisual(Type aType);

protected:
   Type _type;

public:
   explicit Cell() : _type(NONE) {}
   explicit Cell(Type aType) : _type(aType) {}

   Type type() const { return _type; }
   bool freeToMoveIn();

   Visual visual() const override { return toVisual(_type); }
}; // struct Cell

using RawCell = Cell*; // Нет смысла городить optional/reference_wrapper
using Cells = vector<Cell>;


/** Игровое поле. */
class Field
{
protected:
   Cells    _cells;
   Items    _items;
   Units    _units;
   size_t   _w; // h гораздо удобней подсчитывать на лету.
   int      _dx; // Отрицательное значение задаёт автоцентрирование по x.
   int      _dy; // Отрицательное значение задаёт автоцентрирование по y.
   /** Внутреннее имя. Поле в первую очередь идентифицируется по имени,
    *   и лишь во вторую - по координатам. Это связано с тем,
    *   что не все поля расположены на глобальной карте. */
   string   _name;
   string   _desc; // Показывается в интерфейсе.

public:
   static constexpr size_t DEFAULT_W = 80;
   static constexpr size_t DEFAULT_H = 18;

   struct Source
   {
      unsigned int w;
      unsigned int h;
      const char* data;
   };

public:
   explicit Field()=default;
   Field(const Field&)=default;
   explicit Field(const string& aName, const string& aDesc, const Source& src,
                  const Items& aItems, const Units& aUnits,
                  int aDx = -1, int aDy = -1);
   explicit Field(const string& aName, const string& aDesc, size_t aW, size_t aH);

   Field& operator=(const Field&)=default;

   size_t w() const { return _w; }
   size_t h() const { return _cells.size() / _w; }
   int dx() const { return _dx; }
   int dy() const { return _dy; }
   string name() const { return _name; }
   string desc() const { return _desc; }

   Items&         items() { return _items; }
   Items          itemsAt(const int x, const int y, const Item::Pickability pickability) const;
   RawCell        cell(const int x, const int y);
   RawCell        screenCell(const int screenX, const int screenY);
   PItem          topItemAt(const int x, const int y, const bool visibleOnly = true);
   PItem          screenTopItemAt(const int screenX, const int screenY, const bool visibleOnly = true);
   PItem          anyOf(const Item::Type aType);
   void           removeItem(const PItem pItem);

   Units&         units() { return _units; }
   RawUnit        topUnitAt(const int x, const int y, bool visibleOnly = true);
   RawUnit        screenTopUnitAt(const int screenX, const int screenY, const bool visibleOnly = true);
   bool           makeCorpse(RawUnit u);

   bool           tryToOpen(const int aX, const int aY, const Items& inventory, string& reason);
   bool           tryToClose(const int aX, const int aY, string& reason);
}; // class Field

using Fields = std::map<string, Field>;


/** Репозиторий полей игрового мира, включая автосгенерённые. */
class World
{
public:
   using PredefinedLoader = std::function<optional<Field>(const string name)>;
   using GlobalMapLoader = std::function<const Field::Source&()>;
   using SymbolToName = std::function<cstring(char8_t)>;

   using FieldNameToGlobalXY = std::function<tuple<int, int>(cstring& name)>;
   FieldNameToGlobalXY fieldNameToGlobalXY;

   using GlobalXYToFieldName = std::function<cstring(int, int)>;
   GlobalXYToFieldName globalXYToFieldName;

   static constexpr size_t W = 80; // Ширина мира в полях.
   static constexpr size_t H = 21; // Высота мира в полях.

   /** Покоординатное хранение полей. */
   struct Explored
   {
      string   name;
      bool     explored = false;
   };

private:
   /** Хранилище модифицированных полей.
    * Модификацией считается любое изменение состояния (в том числе, даже просто посещение).
    * Такие поля персистентны. Прочие поля подгружаются из ресурсов либо автогенерятся. */
   Fields _fields;

   Explored _explored[H][W];

   GlobalMapLoader   globalMapLoader;
   SymbolToName      symbolToName;
   PredefinedLoader  predefinedLoader;

   static size_t _fieldSuffix;

public:
   explicit World() {}

   Fields& fields() { return _fields; }

   void setGlobalMapLoaders(GlobalMapLoader aGlobalMapLoader, SymbolToName aSymbolToName);
   void setPredefinedLoader(PredefinedLoader aLoader)
   {
      predefinedLoader=aLoader;
   }

   Explored explored(int x, int y) const { return _explored[y][x]; }
   bool isExplored(int x, int y) const { return explored(x, y).explored; }
   void explore(cstring& aName);
   int exploredCount() const;
   tuple<int, int> fieldNameToXY(cstring& name);

   /** Сохранить поле в репозитории. */
   void field(const string aName, const Field& aField) { _fields[aName] = aField; }
   /** Получить поле: либо из репозитория, либо с диска, либо из ресурсов. */
   optional<Field> field(cstring& aName);
   /** Сгенерировать поле. */
   Field generateField(string& newName, size_t aW, size_t aH);
}; // class World


class Ds
{
public:
   static constexpr int W = 80;
   static constexpr int H = 21;

   enum Action
   {
      WAIT,

      STEP_L,
      STEP_R,
      STEP_U,
      STEP_D,
      STEP_LU,
      STEP_LD,
      STEP_RU,
      STEP_RD,

      ATTACK_L,
      ATTACK_R,
      ATTACK_U,
      ATTACK_D,
      ATTACK_LU,
      ATTACK_LD,
      ATTACK_RU,
      ATTACK_RD,

      OPEN_L,
      OPEN_R,
      OPEN_U,
      OPEN_D,
      OPEN_LU,
      OPEN_LD,
      OPEN_RU,
      OPEN_RD,

      CLOSE_L,
      CLOSE_R,
      CLOSE_U,
      CLOSE_D,
      CLOSE_LU,
      CLOSE_LD,
      CLOSE_RU,
      CLOSE_RD,

      SHOOT_L,
      SHOOT_R,
      SHOOT_U,
      SHOOT_D,
      SHOOT_LU,
      SHOOT_LD,
      SHOOT_RU,
      SHOOT_RD
   };

   struct Direction
   {
      int x;
      int y;
   };

   using Directions=vector<Direction>;

   World    world;
   Unit     hero;
   Field    field;

private:
   static mt19937 rng;

   void loadField(cstring& aFieldName);

public:
   Ds();

   void setFieldByName(cstring& aName);
   void setField(const Field& aField);
   bool placeHero (); // Начальное расположение - в клетку, отмеченную Item::PLACEHOLDER.
   void teleportToItem(cstring& aField, const string& aItem);
   void teleportToField(cstring& aField, int aFieldX, int aFieldY, const int x, const int y);

   bool heroAction(const Action a, string& reason);

   Items itemsAtHero(const Item::Pickability pickability = Item::PICKABLE);
   PItem doorAtHero();
   bool drop(PItem item, const int x, const int y);
   bool doorable(const Items& items);
   bool pickable(const Items& items);
   bool pickableAtHeroPosition();

   bool attack(Unit& attacker, const int aX, const int aY, string& reason);
   string mind(Unit& u);

   Directions  gatherDirections(const Unit& u);
   bool        canHeroMoveIn(const int aX, const int aY, string& reason);
   bool        canHeroTransit(const int aX, const int aY,
                              string& newFieldName, int& newFieldX, int& newFieldY,
                              int& newHeroX, int& newHeroY);
   bool        canNpcMoveIn(const int aX, const int aY);

   static Direction toDirection(Action a);
   static int random(int lo, int hi) { return uniform_int_distribution<int>(lo, hi)(rng); }
   static string toText(const Effects& effects, const string& valueAffix = "", const string& glue = "\n");

   void dump();
}; // class Ds

} // namespace ds

#endif // DS_H
