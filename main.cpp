#include "ri/ri_curses.h"
#include "ds/ds.h"
#include "images.h"
#include "fields.h"

#include <algorithm>
#include <sstream>
#include <iomanip>
#include <thread>
#include <chrono>

using namespace rl_interface;
using namespace ds;

#define DEVELOPMENT (true)
//#define DEVELOPMENT (false)

class Game
{
public:
   using R=RiCurses;

   static constexpr int W=80;
   static constexpr int H=25;

private:
   Ds ds;
   R  ri;

public:
   Game() : ri(H, W, true)
   {
      if(!ri)
         exit(1);

      ds.world.setGlobalMapLoaders( fields::globalMap, fields::symbolToName );
      ds.world.setPredefinedLoader( fields::predefinedField );
      ds.world.fieldNameToGlobalXY = fields::fieldNameToGlobalXY;
   }

   string startingField(const Unit::Type aType) const
   {
      if( aType<Unit::ENGINEER || aType>Unit::UNITOLOGIST )
         return "";

      static cstring vu[]=
      {
         fields::MY_WORKSHOP, // ENGINEER
         fields::MY_BARRAKS,  // SOLDIER,
         fields::MY_HOSPITAL, // MEDIC,
         fields::MY_LAB,      // SCIENTIST,
         fields::MY_CELLAR,   // COSSACK,
         fields::MY_CHURCH    // UNITOLOGIST,
      };
      return vu[aType-1];
   }

   void showWindow( void(Game::*f)() )
   {
      ri.storeViews();
      (this->*f)();
      ri.restoreViews();
      ri.update();
   }

   bool showWindow( bool(Game::*f)() )
   {
      ri.storeViews();
      bool result = (this->*f)();
      ri.restoreViews();
      ri.update();
      return result;
   }

   void start()
   {
#if DEVELOPMENT
      windowGame();
#else
      windowTitle();
#endif
   }

   void windowTitle()
   {
      static cstring
            N_BACK    ="back",
            N_PICTURE ="picture",
            N_TEXT1   ="text1",
            N_TEXT2   ="text2",
            N_TEXT3   ="text3",
            N_TITLE   ="title",
            N_MENU    ="menu",
            N_NEW     ="new",
            N_CONTINUE="continue",
            N_ABOUT   ="about",
            N_OPTIONS ="options",
            N_HELP    ="help",
            N_QUIT    ="quit";

      static cstring
            sText1    ="the year is 2515",
            sText2    ="the Earth is overrun by necromorphs",
            sText3    ="SURVIVE. I DARE YOU",
            sTitle    ="DEAD SPACE ROGUE",
            sNew      ="(n) New game",
            sContinue ="(c) Continue",
            sQuit     ="(q) Quit",
            sOptions  ="(o) Options",
            sHelp     ="(h) Help",
            sAbout    ="(a) About";

      ri.clearViews();

      ri.addView(N_BACK, new View(0, 0, W, H-3, R::BLACK, R::RED));
      ri.addView(N_PICTURE, new ImageView(2, 1, images::titleW, images::title));
      ri.setViewParent(N_PICTURE, N_BACK);

      ri.addView(N_TEXT1, new Label(W-View::fsize(sText1)-1, 1, R::BLACK, R::RED, sText1, false));
      ri.addView(N_TEXT2, new Label(W-View::fsize(sText2)-1, 2, R::BLACK, R::RED, sText2, false));
      ri.addView(N_TEXT3, new Label(W-View::fsize(sText3)-1, H/2-2, R::BLACK, R::RED, sText3, true));
      ri.setViewParent(N_TEXT1, N_BACK);
      ri.setViewParent(N_TEXT2, N_BACK);
      ri.setViewParent(N_TEXT3, N_BACK);

      View* menu=new View(0, H-3, W, 3, R::RED, R::BLACK);
      menu->filler=' ';
      ri.addView(N_MENU, menu);

      ri.addView(N_TITLE, new Label( (W-View::fsize(sTitle))/2, 1, R::WHITE, R::RED, sTitle));
      ri.setViewParent(N_TITLE, N_MENU);

      ri.addView(N_NEW, new Label(1, 0, R::RED, R::WHITE, sNew, true));
      ri.setViewParent(N_NEW, N_MENU);

      //TODO Проверять наличие сейвов.
      ri.addView(N_CONTINUE, new Label(1, 1, R::RED, R::WHITE, sContinue, false));
      ri.setViewParent(N_CONTINUE, N_MENU);

      ri.addView(N_QUIT, new Label(1, 2, R::RED, R::WHITE, sQuit, true));
      ri.setViewParent(N_QUIT, N_MENU);

      int rightSize = max( {View::fsize(sHelp),
                            View::fsize(sAbout),
                            View::fsize(sOptions)});

      ri.addView(N_OPTIONS, new Label(W-rightSize-1, 0, R::RED, R::WHITE, sOptions, true));
      ri.setViewParent(N_OPTIONS, N_MENU);

      ri.addView(N_HELP, new Label(W-rightSize-1, 1, R::RED, R::WHITE, sHelp, true));
      ri.setViewParent(N_HELP, N_MENU);

      ri.addView(N_ABOUT, new Label(W-rightSize-1, 2, R::RED, R::WHITE, sAbout, true));
      ri.setViewParent(N_ABOUT, N_MENU);

      for(;;)
      {
         ri.update();

         int c=ri.key();
         switch(c)
         {
         case  'n':
            if( showWindow( &Game::windowNewGame ) )
               showWindow( &Game::windowGame );
            break;

         case  'c': return; //TODO
         case  'q': return;
         case  'o': showWindow( &Game::windowOptions ); break;
         case  'h': showWindow( &Game::windowHelp ); break;
         case  'a': showWindow( &Game::windowAbout ); break;

         default: //TODO: Быстрофикс для кодов клавиш.
            ri.asLabel(N_TITLE)->text = to_string(c);
            ri.update();
            break;
         }
      }
   }

   bool windowNewGame()
   {
      static cstring
            N_HEADER  ="header",
            N_CLASS   ="class",
            N_NAME    ="name",
            N_YOU     ="you",
            N_HAPPY   ="happy";

      static cstring
            sHeader ="The story begins. Choose your destiny (or press [~TAB~] to return):                 ",
            sClass = "Class:\n"
                     "[~a~] Engineer    - Invent things, build things, use things.\n"
                     "[~b~] Soldier     - It's all about guns. Big guns.\n"
                     "[~c~] Medic       - Everyone needs one.\n"
                     "[~d~] Scientist   - Scientia potentia est.\n"
                     "[~e~] Unitologist - Altman be praised.\n"
                     "\n"
                     "Gender:\n"
                     "[~f~] Male\n"
                     "[~g~] Female\n"
                     "\n"
                     "[~h~] Name: \n"
                     "\n"
                     "You are:\n",
            sHappy= "Press [~ENTER~] to go on with this character";

      auto nameView=new Label(10, 11, Unit::MAX_NAME+2, R::BLACK, R::WHITE, ds.hero.name(), false);
      auto youView=new Label(0, 15, W, R::BLACK, R::YELLOW, "Sample", true);

      ri.addView(N_HEADER, new Label(0, 0, R::RED, R::WHITE, sHeader, false));
      ri.addView(N_CLASS, new TextView(0, 1, W, H-1, R::BLACK, R::WHITE, sClass, false));
      ri.addView(N_NAME, nameView);
      ri.setViewParent(N_NAME, N_CLASS);
      ri.addView(N_YOU, youView);
      ri.addView(N_HAPPY, new Label(0, H-1, W, R::BLACK, R::WHITE, sHappy, false));

      auto updateUnitView = [&]
      {
         string s = Unit::toString(ds.hero.gender()) + " " +
                    Unit::toString(ds.hero.type()) + " ~named~ " +
                    ds.hero.name();
         youView->text=s;
         ri.update();
      };

      auto editName = [&]
      {
         static const string CARET="|";

         auto trim = [](string s)
         {
            s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
            s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
            return s;
         };

         auto nameIsGood = [](string name)
         {
            return name.size()>=1 && name.size()<=Unit::MAX_NAME;
         };

         string name=ds.hero.name();

         nameView->highlight=true;
         nameView->text = name + CARET;
         ri.update();

         for(;;)
         {
            int c=ri.key();
            if( (isalnum(c) || ' '==c) && name.size()<=Unit::MAX_NAME )
            {
               name += c;
               nameView->text = name + CARET;
            }
            else if(key::BACKSPACE==c)
            {
               name=name.substr(0, name.size()-1);
               nameView->text = name + CARET;
            }
            else if(key::ENTER==c && nameIsGood( trim(name) ))
            {
               ds.hero.name( trim(name) );
               break;
            }

            if( nameIsGood(trim(name)) )
            {
               nameView->foreground=R::WHITE;
               ri.update();
            }
            else
            {
               nameView->foreground=R::RED;
               ri.update();
            }
         }

         nameView->highlight=false;
         nameView->text = ds.hero.name();
         updateUnitView();
      };

      for(;;)
      {
         updateUnitView();

         int c=ri.key();
         switch(c)
         {
         case  'a': ds.hero.type(Unit::ENGINEER   ); break;
         case  'b': ds.hero.type(Unit::SOLDIER    ); break;
         case  'c': ds.hero.type(Unit::MEDIC      ); break;
         case  'd': ds.hero.type(Unit::SCIENTIST  ); break;
         case  'e': ds.hero.type(Unit::UNITOLOGIST); break;

         case  'f': ds.hero.gender(Unit::MALE  ); break;
         case  'g': ds.hero.gender(Unit::FEMALE); break;

         case  'h': editName(); updateUnitView(); break;

         case  key::TAB: return false;

         case  key::ENTER:
            ds.placeHero();
            return true;
         }
      }
   }

   void windowOptions()
   {
      static cstring N_OPTIONS="options";
      static cstring sOptions ="Options screen (press ~TAB~ to return)";

      ri.addView(N_OPTIONS, new Label(1, 1, R::BLUE /*ACK*/, R::BLACK, sOptions));
      ri.update();

      while( key::TAB!=ri.key() );
   }

   void windowHelp()
   {
      static cstring N_HELP="help";
      static cstring sHelp ="Help screen (press ~TAB~ to return)";

      ri.addView(N_HELP, new Label(1, 1, R::BLACK, R::BLACK, sHelp));
      ri.update();

      while( key::TAB!=ri.key() );
   }

   void windowAbout()
   {
      static cstring N_ABOUT="about";
      static cstring sAbout ="About screen (press ~TAB~ to return)";

      ri.addView(N_ABOUT, new Label(1, 1, R::BLACK, R::BLACK, sAbout));
      ri.update();

      while( key::TAB!=ri.key() );
   }

   void windowInventory()
   {
      static cstring
            N_TOP="top",
            N_EQUIPPED="equipped",
            N_LIST="list",
            N_PANEL="panel",
            N_BOTTOM="bottom",
            N_WEIGHT="wgt";

      static cstring
            sInventory ="Inventory",
            sTab ="[~TAB~] to close";

      int spaces = W - View::fsize(sInventory) - View::fsize(sTab);
      string topLine = sInventory + string(spaces, ' ') + sTab;
      auto top=new Label(0, 0, W, R::BLUE, R::WHITE, topLine, false);

      auto equipped=new TextView(0, 1, W, 2, R::BLACK, R::WHITE, "Hands :\nOutfit:");
      auto list=new TextView(0, 4, W/2-1, H-5, R::BLACK, R::WHITE, "Backpack");
      auto panel=new TextView(W/2+1, 4, W/2-1, H-5, R::BLACK, R::WHITE, "Panel");
      auto bottom=new Label(0, H-1, W, R::BLUE, R::WHITE, "Bottom line", false);
      auto weight=new Label(bottom->w-11, 0, R::BLUE, R::WHITE, "Wgt: 999 kg", false);

      ri.addView(N_TOP, top);
      ri.addView(N_EQUIPPED, equipped);
      ri.addView(N_LIST, list);
      ri.addView(N_PANEL, panel);
      ri.addView(N_BOTTOM, bottom);
      ri.addView(N_WEIGHT, weight);
      ri.setViewParent(N_WEIGHT, N_BOTTOM);

      const int FIRST_LETTER = 'a';
      const int LAST_LETTER = FIRST_LETTER + list->h - 2;
      const size_t PAGE_SIZE = LAST_LETTER - FIRST_LETTER;
      size_t page=0;

      constexpr int ITEM_NONE=-3;
      constexpr int ITEM_WEAPON=-1;
      constexpr int ITEM_OUTFIT=-2;
      int itemSelected=ITEM_NONE;

      Items& inventory=ds.hero.inventory();
      PItem weapon=ds.hero.weapon();
      PItem outfit=ds.hero.outfit();

      auto updateBottom = [&]
      {
         bottom->text = "Page " + to_string(page+1) + "/" +
                        to_string(inventory.size()/PAGE_SIZE + 1) +
                        " [~+~/~-~], [~ENTER~] to un/equip, [~SPACE~] to drop";
      };

      auto updateItems = [&]
      {
         equipped->text =
               (weapon->nothing() ?
                   "[1] Empty hands" :
                   "[~1~] Hands : " + weapon->name()) + "\n" +
               (outfit->nothing() ?
                   "[2] Naked" :
                   "[~2~] Outfit: " + outfit->name());

         list->text = "Backpack:\n";
         for(size_t i=0; i<=PAGE_SIZE; ++i)
         {
            if( page*PAGE_SIZE+i >= inventory.size() )
               break;

            string s="[~" + string(1, i+FIRST_LETTER) + "~] " +
                     inventory[page*PAGE_SIZE+i]->name() + "\n";
            list->text += s;
         }
      };

      auto updatePanel = [&]
      {
         switch(itemSelected)
         {
         case  ITEM_NONE: panel->text="No item selected"; break;

         case  ITEM_WEAPON:
            panel->text = "~" + weapon->name() + "~: " +
                          weapon->desc() + "\n\n" +
                          Ds::toText(weapon->effects(), "~");
            break;

         case  ITEM_OUTFIT:
            panel->text = "~" + outfit->name() + "~: " +
                          outfit->desc() + "\n\n" +
                          Ds::toText(outfit->effects(), "~");
            break;

         default:
            {
               PItem item = inventory[itemSelected];
               panel->text = "~" + item->name() + "~: " +
                             item->desc() + "\n\n" +
                             Ds::toText(item->effects(), "~");
            }
            break;
         }
      };

      for(;;)
      {
         updateItems();
         updatePanel();
         updateBottom();
         ri.update();

         int c=ri.key();
         switch(c)
         {
         case  '+':
            itemSelected=ITEM_NONE;
            if( inventory.size()/PAGE_SIZE > page )
               page++;
            break;

         case  '-':
            itemSelected=ITEM_NONE;
            if(page>0)
               page--;
            break;

         case  key::ENTER: // Un/equip.
            switch(itemSelected)
            {
            case  ITEM_NONE: break;

            case  ITEM_WEAPON:
               inventory.push_back(weapon);
               weapon=make_shared<Item>(new Item(Item::NONE));
               itemSelected=ITEM_NONE;
               break;

            case  ITEM_OUTFIT:
               inventory.push_back(outfit);
               outfit=make_shared<Item>(new Item(Item::NONE));
               itemSelected=ITEM_NONE;
               break;

            default:
               {
                  PItem item = inventory.at(itemSelected);
                  if( item->weaponable() )
                  {
                     if( !weapon->nothing() )
                        inventory.push_back(weapon);
                     weapon = item;
                     inventory.erase(inventory.begin()+itemSelected);
                     itemSelected=ITEM_NONE;
                  }
                  else if( item->outfitable() )
                  {
                     if( !outfit->nothing() )
                        inventory.push_back(outfit);
                     outfit = item;
                     inventory.erase(inventory.begin()+itemSelected);
                     itemSelected=ITEM_NONE;
                  }
               }
               break;
            }
            break;

         case  key::SPACE: // Drop
            switch(itemSelected)
            {
            case  ITEM_NONE: break;

            case  ITEM_WEAPON:
               ds.drop(weapon, ds.hero.x(), ds.hero.y());
               weapon=make_shared<Item>(new Item(Item::NONE));
               itemSelected=ITEM_NONE;
               break;

            case  ITEM_OUTFIT:
               ds.drop(outfit, ds.hero.x(), ds.hero.y());
               outfit=make_shared<Item>(new Item(Item::NONE));
               itemSelected=ITEM_NONE;
               break;

            default:
               {
                  PItem item = inventory.at(itemSelected);
                  ds.drop(item, ds.hero.x(), ds.hero.y());
                  inventory.erase(inventory.begin()+itemSelected);
                  itemSelected=ITEM_NONE;
               }
               break;
            }
            break;

         case  key::TAB : return;

         case  '1': // Weapon
            if( !weapon->nothing() )
               itemSelected=ITEM_WEAPON;
            break;

         case  '2': // Outfit
            if( !outfit->nothing() )
               itemSelected=ITEM_OUTFIT;
            break;

         default:
            if(c>=FIRST_LETTER && c<=LAST_LETTER)
            {
               int index = page*PAGE_SIZE + c-FIRST_LETTER;
               if( index>=0 && index<int(inventory.size()) )
                  itemSelected=index;
               else
                  itemSelected=ITEM_NONE;
            }
            break;
         }
      }
   }

   void windowMap()
   {
      //TODO: Legend, selection of fields with info.

      static cstring
            N_TOP="top",
            N_WORLD="world",
            N_STAT="stat";

      static cstring sMap ="World map (press ~TAB~ to return)";

      auto access = [&](int x, int y)
      {
         bool explored = ds.world.isExplored(x, y);
         return Pixel{explored ? '#' : '.', R::BLACK, R::BLUE, !explored};
      };

      auto top=new Label(0, 0, W, R::BLUE, R::WHITE, sMap, false);
      auto worldMap=new FieldView(access, 0, 1, World::W, World::H);
      auto stat=new Label(0, H-1, W, R::BLUE, R::WHITE,
                          "Sectors explored: "+to_string(ds.world.exploredCount()), false);
      ri.addView(N_TOP, top);
      ri.addView(N_WORLD, worldMap);
      ri.addView(N_STAT, stat);
      ri.update();

      while( key::TAB!=ri.key() );
   }

   void windowPickup()
   {
      static cstring
            N_TOP="top",
            N_LIST="list",
            N_BOTTOM="bottom",
            N_WEIGHT="wgt";

      static cstring
            sPickup ="Pick up",
            sTab ="[~TAB~] to close";

      int spaces = W - View::fsize(sPickup) - View::fsize(sTab);
      string topLine = sPickup + string(spaces, ' ') + sTab;
      auto top=new Label(0, 0, W, R::BLUE, R::WHITE, topLine, false);
      auto list=new TextView(0, 1, W, H-2, R::BLACK, R::WHITE, "Ground");
      //      auto panel=new TextView(W/2+1, 4, W/2-1, H-5, R::BLACK, R::WHITE, "Panel");
      auto bottom=new Label(0, H-1, W, R::BLUE, R::WHITE, "Bottom line", false);
      auto weight=new Label(bottom->w-11, 0, W, R::BLUE, R::WHITE, "Wgt: 999 kg", false);

      ri.addView(N_TOP, top);
      ri.addView(N_LIST, list);
      //      ri.addView(N_PANEL, panel);
      ri.addView(N_BOTTOM, bottom);
      ri.addView(N_WEIGHT, weight);
      ri.setViewParent(N_WEIGHT, N_BOTTOM);

      const int FIRST_LETTER = 'a';
      const int LAST_LETTER = FIRST_LETTER + list->h - 1;
      const int PAGE_SIZE = LAST_LETTER - FIRST_LETTER;
      int page=0;

      Items& inventory=ds.hero.inventory();
      vector<PItem> items;

      auto updateUI = [&]
      {
         items=ds.itemsAtHero();

         list->text.clear();
         for(int i=0; i<=PAGE_SIZE; ++i)
         {
            if( page*PAGE_SIZE+i >= int(items.size()) )
               break;

            PItem item = items[page*PAGE_SIZE+i];
            string text = item->desc().empty() ?
                             (item->name() + " (no description provided)") :
                             item->desc();

            string s = "[~" + string(1, i+FIRST_LETTER) + "~] " +
                       text + "\n";
            list->text += s;
         }

         bottom->text = "Page " + to_string(page+1) + "/" +
                        to_string(items.size()/PAGE_SIZE + 1) +
                        " [~+~/~-~], [~letter~] to pick up, [~ENTER~] to grab all";
         ri.update();
      };

      for(;;)
      {
         updateUI();

         int c=ri.key();
         switch(c)
         {
         case  key::TAB: return;

         case  '+':
            if( int(items.size()/PAGE_SIZE) > page )
               page++;
            break;

         case  '-':
            if(page>0)
               page--;
            break;

         case  key::ENTER:
            while( !items.empty() )
            {
               PItem p = items.front();
               inventory.push_back(p);
               ds.field.removeItem(p);
               items = ds.itemsAtHero();
            }
            return;

         default:
            if(c>=FIRST_LETTER && c<=LAST_LETTER)
            {
               int index = page*PAGE_SIZE + c-FIRST_LETTER;
               if( index>=0 && index<int(items.size()) )
               {
                  PItem p = items[index];
                  inventory.push_back(p);
                  ds.field.removeItem(p);
                  if( ds.itemsAtHero().empty() ) // Собрали последний - можно выходить.
                     return;
               }
            }
            break;
         }
      }
   }

   void windowGame()
   {
      static cstring
            N_TOP    ="top",
            N_FIELD  ="field",
            N_BOTTOM ="bottom",
            N_HP     ="hp",
            N_WEAPON ="weapon",
            N_OUTFIT ="outfit",
            N_LOG ="log";

      static cstring
            sTab="[~TAB~] for menu",
            sInitialLog="Here is what you call 'home'... for a time being.\nWell, it has walls and a roof.";

      // Задаём стартовые условия.
#if DEVELOPMENT
      ds.setFieldByName(fields::MY_WORKSHOP_STREET);
#else
      ds.setFieldByName(startingField(ds.hero.type()));
#endif
      ds.placeHero();
      //TODO: демонстрация: ds.hero.effects().push_back( {Effect::DAMAGE_OVER_TIME, 2} );

      // Колбэк доступа к визуальным данным поля.
      auto access = [&](int x, int y)
      {
         Visual v=Cell::toVisual(Cell::NONE); // По умолчанию...
         if( RawCell c=ds.field.screenCell(x, y) ) // ...и небольшое уточнение.
            v=c->visual();
         bool highlight=false;

         if( ds.hero.here(x-ds.field.dx(), y-ds.field.dy()) )
         {
            v.symbol=HERO_SYMBOL;
            v.foreground=R::YELLOW;
            highlight=true;
         }
         else if( RawUnit unit=ds.field.screenTopUnitAt(x, y, true) )
         {
            Visual uv=unit->visual();
            v.symbol=uv.symbol;
            v.foreground=uv.foreground;
            highlight=true;
         }
         else if( PItem item = ds.field.screenTopItemAt(x, y, true) )
         {
            Visual iv = item->visual();
            v.symbol = iv.symbol;
            v.foreground = iv.foreground;
            highlight = true;
         }

         return Pixel{v.symbol, v.background, v.foreground, highlight};
      };

      // Верхняя полоска: <Название карты>__<SPACE for menu>
      auto top=new Label(0, 0, W, R::BLUE, R::WHITE, "<top line>", false);
      // Игровое поле.
      auto field=new FieldView(access, 0, 1, Ds::W, Ds::H);
      // Нижняя панель.
      auto bottom=new View(0, Ds::H+1, W, 3, R::BLUE, R::WHITE, ' ');
      auto labelHp      =new Label(0, 0, 12, R::BLUE, R::WHITE, "HP: ~999~/999", false);
      auto labelWeapon  =new Label(0, 1, 25, R::BLUE, R::WHITE, "Plasma cutter1234", false);
      auto labelOutfit  =new Label(0, 2, 25, R::BLUE, R::WHITE, "Wearing nothing, a.k.a. naked", false);
      auto log=new TextView(27, 0, W-27, bottom->h, R::BLACK, R::WHITE, sInitialLog, false);

      ri.addView(N_TOP, top);
      ri.addView(N_FIELD, field);
      ri.addView(N_BOTTOM, bottom);

      ri.addView(N_HP    , labelHp);
      ri.addView(N_WEAPON, labelWeapon);
      ri.addView(N_OUTFIT, labelOutfit);
      ri.setViewParent(N_HP    , N_BOTTOM);
      ri.setViewParent(N_WEAPON, N_BOTTOM);
      ri.setViewParent(N_OUTFIT, N_BOTTOM);

      ri.addView(N_LOG, log);
      ri.setViewParent(N_LOG, N_BOTTOM);

      // Обновление интерфейса.
      auto updateCharacterInfo = [&]
      {
         Unit& u=ds.hero;
         Unit::Attributes& a=u.attributes();

         ostringstream weapon;
         weapon<<"~"<<
                 u.rangeAttackOverall() <<
                 "~|~" <<
                 u.closeAttackOverall() <<
                 "~|~" <<
                 u.rangeCritOverall() <<
                 "~|~" <<
                 u.closeCritOverall() <<
                 "~ (" <<
                 (u.weapon()->nothing() ? "empty hands" : u.weapon()->name()) <<
                 ")";

         ostringstream outfit;
         outfit<<"~"<<
                 u.rangeDefenceOverall() <<
                 "~|~" <<
                 u.closeDefenceOverall() <<
                 "~|~" <<
                 u.rangeEvasionOverall() <<
                 "~|~" <<
                 u.closeEvasionOverall() <<
                 "~ (" <<
                 (u.outfit()->nothing() ? "naked" : u.outfit()->name()) <<
                 ")";

         if( ds.hero.dead() )
         {
            labelHp->foreground=R::RED;
            labelHp->text="~DEAD~";
         }
         else
            labelHp->text = "HP: ~" + to_string(a.hp) + "~/" + to_string(a.maxHp);
         labelWeapon->text = weapon.str();
         labelOutfit->text = outfit.str();

         // Верхняя полоска: <(x-y)>_<Название карты>__<SPACE for menu>
         {
            ostringstream pos;
            pos<<"("<<ds.hero.x()<<"-"<<ds.hero.y()<<") ";
            size_t spaces = W - View::fsize(pos.str()) - View::fsize(ds.field.desc()) - View::fsize(sTab);
            string topLine = pos.str() + ds.field.desc() + string(spaces, ' ') + sTab;
            top->text=topLine;
         }
         ri.update();
      };

      // Действия НИП.
      auto npcs = [&]
      {
         string events;

         Units& units=ds.field.units();
         for(auto& u: units)
            events += ds.mind(u);

         return events;
      };

      // "Такт" - отработка изменений в пространстве, статусов и т.п.
      auto tick = [&]
      {
         string events = npcs();

         // Состояние протагониста.
         Items items=ds.field.itemsAt(ds.hero.x(), ds.hero.y(), Item::ANY);

         // Собственные эффекты.
         int selfDamage = ds.hero.effects().accumulate(Effect::DAMAGE_OVER_TIME);
         if(selfDamage)
         {
            events += "Bleeding. Lost ~" + to_string(selfDamage) + "~ HP. ";
            ds.hero.attributes().hp -= selfDamage;
         }

         // От предметов на полу.
         int itemsDamage=0;
         for(auto ii: items)
            itemsDamage += ii->effects().accumulate(Effect::DAMAGE_ON_TOUCH); // Которых касаемся.

         if(itemsDamage)
         {
            events += "Ouch. Lost ~" + to_string(itemsDamage) + "~ HP. ";
            ds.hero.attributes().hp -= itemsDamage;
         }

         if( ds.hero.dead() )
            events += "You are dead. Congrats.";
         else // Остальное надо показывать, только если ещё жив.
         {
            if( ds.pickable(items) )
               events += "Something's on the floor, [~p~]ick it up. ";
            if( ds.doorable(items) )
               events += "Here is an open [~d~]oor, you know. ";
         }

         if( !events.empty() ) // Переписываем лог, только если есть, что сказать.
         {
            log->text = log->text.empty() ? events : log->text+" "+events;
            ri.update();
         }
      };

      // Отработка действий.
      auto action = [&](Ds::Action a)
      {
         string reason;
         ds.heroAction(a, reason);
         log->text=reason;
         tick();
      };

      // Поднятие предметов с пола.
      auto pickup = [&]()
      {
         if( ds.pickableAtHeroPosition() )
            showWindow( &Game::windowPickup );
         else
         {
            log->text = "Nothing to pick up here.";
            ri.update();
         }
      };

      auto door = [&]()
      {
         PItem pDoor=ds.doorAtHero();
         if(!pDoor)
         {
            log->text = "There is no usable door here.";
            ri.update();
            return;
         }

         using namespace tags;

         // Дверь = телепорт.
         if( pDoor->hasTagId(TELEPORT) )
         {
            try
            {
               auto[field, item] = pDoor->parseTagTeleport();
//               log->text=field+" "+item;
               ds.teleportToItem(field, item);
            }
            catch(...)
            {
               log->text+=" ERROR: Bad door tag. Probably wrong field or item data.";
               ri.update();
            }
         }
      };

      // Главный цикл.
      for(;;)
      {
         updateCharacterInfo();

         switch( ri.key() )
         {
         case  key::SPACE  : action(Ds::WAIT); break;

         case  key::LEFT   : action(Ds::STEP_L); break;
         case  key::RIGHT  : action(Ds::STEP_R); break;
         case  key::UP     : action(Ds::STEP_U); break;
         case  key::DOWN   : action(Ds::STEP_D); break;
         case  key::HOME   : action(Ds::STEP_LU); break;
         case  key::END    : action(Ds::STEP_LD); break;
         case  key::PGUP   : action(Ds::STEP_RU); break;
         case  key::PGDOWN : action(Ds::STEP_RD); break;

         case  'i': showWindow( &Game::windowInventory ); break;
         case  'h': showWindow( &Game::windowHelp ); break;
         case  'm': showWindow( &Game::windowMap ); break;
         case  'l': break; //TODO: look
         case  'e': break; //TODO: search
         case  'p': pickup(); break;

         case  's':
            {
               log->text="Shoot where? [~arrows~]";
               ri.update();
               switch( ri.key() )
               {
               case  key::LEFT   : action(Ds::SHOOT_L); break;
               case  key::RIGHT  : action(Ds::SHOOT_R); break;
               case  key::UP     : action(Ds::SHOOT_U); break;
               case  key::DOWN   : action(Ds::SHOOT_D); break;
               case  key::HOME   : action(Ds::SHOOT_LU); break;
               case  key::END    : action(Ds::SHOOT_LD); break;
               case  key::PGUP   : action(Ds::SHOOT_RU); break;
               case  key::PGDOWN : action(Ds::SHOOT_RD); break;
               default:
                  break;
               }
               ri.update();
            }
            break;

         case  'a':
            {
               log->text="Attack where? [~arrows~]";
               ri.update();
               switch( ri.key() )
               {
               case  key::LEFT   : action(Ds::ATTACK_L); break;
               case  key::RIGHT  : action(Ds::ATTACK_R); break;
               case  key::UP     : action(Ds::ATTACK_U); break;
               case  key::DOWN   : action(Ds::ATTACK_D); break;
               case  key::HOME   : action(Ds::ATTACK_LU); break;
               case  key::END    : action(Ds::ATTACK_LD); break;
               case  key::PGUP   : action(Ds::ATTACK_RU); break;
               case  key::PGDOWN : action(Ds::ATTACK_RD); break;
               default:
                  break;
               }
            }
            break;

         case  'o':
            {
               log->text="Open where? [~arrows~]";
               ri.update();
               switch( ri.key() )
               {
               case  key::LEFT   : action(Ds::OPEN_L); break;
               case  key::RIGHT  : action(Ds::OPEN_R); break;
               case  key::UP     : action(Ds::OPEN_U); break;
               case  key::DOWN   : action(Ds::OPEN_D); break;
               case  key::HOME   : action(Ds::OPEN_LU); break;
               case  key::END    : action(Ds::OPEN_LD); break;
               case  key::PGUP   : action(Ds::OPEN_RU); break;
               case  key::PGDOWN : action(Ds::OPEN_RD); break;
               default:
                  break;
               }
            }
            break;

         case  'c':
            {
               log->text="Close where? [~arrows~]";
               ri.update();
               switch( ri.key() )
               {
               case  key::LEFT   : action(Ds::CLOSE_L); break;
               case  key::RIGHT  : action(Ds::CLOSE_R); break;
               case  key::UP     : action(Ds::CLOSE_U); break;
               case  key::DOWN   : action(Ds::CLOSE_D); break;
               case  key::HOME   : action(Ds::CLOSE_LU); break;
               case  key::END    : action(Ds::CLOSE_LD); break;
               case  key::PGUP   : action(Ds::CLOSE_RU); break;
               case  key::PGDOWN : action(Ds::CLOSE_RD); break;
               default:
                  break;
               }
            }
            break;

         case  'd': door(); break;

         case  key::TAB: return;
         case  key::F12: ds.dump();
         }

         if( ds.hero.dead() )
         {
            updateCharacterInfo();
            this_thread::sleep_for(chrono::seconds(1));
            while( !ri.key() );
            return;
         }
      }
   }
}; // class Game


int main()
{
   {
      Game g;
      g.start();
   }
   printf("Thanks for visiting!\n");

   return 0;
}
