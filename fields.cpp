#include "fields.h"

#include "ds/tags.h"

#include <iostream>

namespace fields {

using namespace tags;

static const string DOOR_OUT_OF_MY_WORKSHOP="doorOutOfMyWorkshop";
static const string DOOR_INTO_MY_WORKSHOP="doorIntoMyWorkshop";

static const Effects fireEffects { {Effect::DAMAGE_ON_TOUCH, 1} };

Effects gunEffects(int loRange, int hiRange, int loClose, int hiClose)
{
   return Effects
   {
      {Effect::RANGE_ATTACK, Ds::random(loRange, hiRange)},
      {Effect::CLOSE_ATTACK, Ds::random(loClose, hiClose)}
   };
}


// Общие поля.

const Field::Source _myZero=
{
   80, 18,
   "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
   "%%%%%%%%%%.....%%.....%%%%%%%%.%.%%%%...%%%%%%%%%%%...........%%%%%..%%%%%......"
   "%%%%%.%%%................%%..%.................%%%......................%........"
   "%%%%............................................................................"
   "%%%............................................................................."
   "%%.............................................................................."
   "%%.............................................................................."
   "%..............................................................................."
   "%..............................................................................."
   "%..............................................................................."
   "%..............................................................................."
   "%%.............................................................................."
   "%%.............................................................................."
   "%%%............................................................................."
   "%%%............................................................................."
   "%%.............................................................................."
   "%..............................................................................."
   "%..............................................................................."
};


// Поля инженера.

const Field::Source _myWorkshop=
{
   20, 10,
   "####################"
   "#....#.............#"
   "#....#.............#"
   "#....#.............#"
   "#....###.###########"
   "#....#.............#"
   "#....#.............#"
   "#..................#"
   "#....#.............#"
   "########.###########"
};

Items&& _myWorkshopItems()
{
   static Items items
   {
      shared(new Item{Item::PLACEHOLDER       ,  3, 1, "starting point", "", false}),
            shared(new Item{Item::BED               ,  1, 2, "bed", "This is where you sleep."}),
            shared(new Item{Item::FIRE              ,  1, 8, "fire", "Fireplace.", fireEffects}),
            shared(new Item{Item::CONTAINER_CLOSED  , 18, 3, "myWorkshop.chest"}),
            shared(new Item{Item::KEY               , 18, 3, id(MY_WORKSHOP, "key"), "My workshop's key", false}),
            shared(new Item{Item::TABLE             , 15, 1, "workbench"}),
            shared(new Item{Item::DOOR_CLOSED       ,  8, 4, "door"}),
            shared(new Item{Item::DOOR_UP_CLOSED    ,  8, 9, DOOR_OUT_OF_MY_WORKSHOP, "Door outside"}),
            shared(new Item{Item::DOOR_DOWN         , 18, 6, "doorDown", "Basement trapdoor"}),

            //TODO Тестовые
            shared(new Item{Item::WEAPON, 3, 2, "Gun 1", "Gun 1 description", gunEffects(5, 7, 1, 2)}),
            shared(new Item{Item::OUTFIT, 3, 2, "Outfit 1"}),
            shared(new Item{Item::WEAPON, 3, 2, "Gun 2", gunEffects(5, 7, 1, 2)}),
            shared(new Item{Item::OUTFIT, 3, 2, "Outfit 2"}),
            shared(new Item{Item::WEAPON, 3, 2, "Gun 3", gunEffects(5, 7, 1, 2)}),
            shared(new Item{Item::CONTAINER, 3, 2, "UNPICKABLE"})
   };

   items.item(DOOR_OUT_OF_MY_WORKSHOP)->tag( lock(MY_WORKSHOP, DOOR_OUT_OF_MY_WORKSHOP) +
                                             SPLITTER +
                                             teleport(MY_WORKSHOP_STREET, DOOR_INTO_MY_WORKSHOP));
   items.item(id(MY_WORKSHOP, "key"))->tag( lock(MY_WORKSHOP, DOOR_OUT_OF_MY_WORKSHOP) );

   items.item("doorDown")->tag( teleport(MY_WORKSHOP_BASEMENT, "doorUp") );

   return move(items);
}

const Field::Source _myWorkshopBasement=
{
   18, 8,
   "##################"
   "#................#"
   "#................#"
   "#................#"
   "#................#"
   "#................#"
   "#................#"
   "##################"
};

Items&& _myWorkshopBasementItems()
{
   static Items items
   {
      shared(new Item{Item::DOOR_UP, 16, 4, "doorUp", "Trapdoor up"})
   };

   items.item("doorUp")->tag( teleport(MY_WORKSHOP, "doorDown") );

   return move(items);
}

Units&& _myWorkshopBasementUnits()
{
   static Units units
   {
      {Unit::NECRO, Unit::WANDERER, Unit::Wanderer{1, 1, 4, 4, 3}, Unit::MALE, "my%_necro", 2, 3}
   };

   return move(units);
}

const Field::Source _myWorkshopStreet=
{
   80, 18,
   ".###############################################################################"
   ".############*..........._...............###.#.#.###****...**........*.*########"
   ".############...........ww__._...........#####.#####**..................*#######"
   ".......................wwwwwwww.........................................**######"
   ".........................wwwww..........................................*#######"
   "........................................................................*#######"
   "........................................................................########"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   ".........................___....................................___............."
   "#.######.######.######.###___............................___________**.........."
   "##########################.____.............**..*....._____________%%%%........."
   "#############################%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"
};

Items&& _myWorkshopStreetItems()
{
   static Items items
   {
      shared(new Item{Item::DOOR_DOWN, 46, 2, DOOR_INTO_MY_WORKSHOP, "Trapdoor down"})
   };

   items.item(DOOR_INTO_MY_WORKSHOP)->tag( teleport(MY_WORKSHOP, DOOR_OUT_OF_MY_WORKSHOP) );

   return move(items);
}

Units&& _myWorkshopStreetUnits()
{
   static Units units
   {
      {Unit::CIVILIAN, Unit::BYSTANDER, Unit::Bystander{}, Unit::MALE, "civilian1", 70, 11},
      {Unit::NECRO, Unit::BRUTE, Unit::Brute{false}, Unit::MALE, "necro%_brute", 60, 11,
         {
            shared(new Item{Item::WEAPON, 3, 2, "Gun from necro", "Gun from necro description", gunEffects(5, 7, 1, 2)})
         }
      }
   };

   return move(units);
}


// Поля солдата.

const Field::Source _myBarracksStreet=
{
   80, 18,
   "################################################################################"
   "...............................................................................#"
   "...............................................................................#"
   "...............................................................................#"
   "...............................................................................#"
   "...............................................................................#"
   "...............................................................................#"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "################################################################################"
};

Items&& _myBarracksStreetItems()
{
   static Items items
   {
   };

   return move(items);
}

Units&& _myBarracksStreetUnits()
{
   static Units units
   {
   };

   return move(units);
}

optional<Field> predefinedField(const string& name)
{
   map<string, Field> fields
   {
      { MY_WORKSHOP           , Field(MY_WORKSHOP           , "My workshop"         , _myWorkshop        , _myWorkshopItems(), Units{}) },
      { MY_WORKSHOP_BASEMENT  , Field(MY_WORKSHOP_BASEMENT  , "My workshop basement", _myWorkshopBasement, _myWorkshopBasementItems(), _myWorkshopBasementUnits()) },
      { MY_WORKSHOP_STREET    , Field(MY_WORKSHOP_STREET    , "My workshop street"  , _myWorkshopStreet  , _myWorkshopStreetItems(), _myWorkshopStreetUnits()) },

      { MY_BARRACKS_STREET    , Field(MY_BARRACKS_STREET    , "My barracks street"  , _myBarracksStreet  , _myBarracksStreetItems(), _myBarracksStreetUnits()) }
   };

   auto fi=fields.find(name);
   if( fields.end()!=fi )
      return {fi->second};

   return nullopt;
}


//TODO Идея такая: это как бы глобальная карта.
//TODO Часть полей предопределена, часть генерится на лету и запоминается.
//TODO
const Field::Source _globalMap=
{
   World::W, World::H,
   "Z..............................................................................."
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................................"
   "................................................................BW.............."
   "................................................................................"
   "................................................................................"
   "................................................................................"
};

// Соответствие между полем и его символом на глобальной карте.
struct Denotation
{
   char8_t  symbol;
   cstring  name;
};

static const size_t DENOTATIONS_SIZE=3;
static const Denotation denotations[DENOTATIONS_SIZE]=
{
   {'Z', MY_ZERO},
   {'W', MY_WORKSHOP_STREET},
   {'B', MY_BARRACKS_STREET}
};

const Field::Source& globalMap(){ return _globalMap; }

cstring symbolToName(char8_t symbol)
{
   string name;
   for(size_t i=0; i<DENOTATIONS_SIZE; ++i) // God it's ugly. //TODO
   {
      if(denotations[i].symbol==symbol)
      {
         name = denotations[i].name;
         break;
      }
   }
   return name;
}

tuple<int, int> fieldNameToGlobalXY(cstring& name)
{
   //TODO Very ineffective, but it doesn't matter now.
   for(size_t i=0; i<DENOTATIONS_SIZE; ++i)
   {
      if(denotations[i].name==name)
      {
         char8_t symbol = denotations[i].symbol;
         for(size_t j=0; j<World::H; ++j)
            for(size_t k=0; k<World::W; ++k)
            {
               if(_globalMap.data[j*World::W+k]==symbol)
                  return {k, j};
            }
         return {-1, -1};
      }
   }
   return {-1, -1};
}

cstring globalXYToFieldName(int x, int y)
{
   string name;
   if(x>=0 && x<int(World::W) && y>=0 && y<int(World::W))
   {
      char8_t symbol = _globalMap.data[y*World::W+x];
      for(size_t i=0; i<DENOTATIONS_SIZE; ++i) // God it's ugly. //TODO
      {
         if(denotations[i].symbol==symbol)
         {
            name = denotations[i].name;
            break;
         }
      }
   }
   return name;
}

} // namespace fields
